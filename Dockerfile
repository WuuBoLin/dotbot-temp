FROM archlinux:latest
RUN pacman -Syyu --noconfirm opus yt-dlp ffmpeg
COPY target/release/dotbot .
RUN chmod +x ./dotbot
COPY .env .
CMD ["./dotbot"]
