use crate::result::errors::DotError;
use crate::result::DotResult;
use chrono::{DateTime, Duration, NaiveDateTime, NaiveTime, Timelike, Utc};

pub fn duration_hms_to_ms(hms: String) -> DotResult<u32> {
    let parts: Vec<&str> = hms.split(':').collect();
    let timestamp = match parts.len() {
        1 => format!("00:00:{}", parts.first().unwrap()),
        2 => format!("00:{}:{}", parts.first().unwrap(), parts[1]),
        3 => format!("{}:{}:{}", parts.first().unwrap(), parts[1], parts[2]),
        _ => return Err(DotError::FailParseTime),
    };

    let Ok(naive_time) = NaiveTime::parse_from_str(&timestamp, "%H:%M:%S") else {
        return Err(DotError::FailParseTime);
    };

    let duration = Duration::seconds(naive_time.num_seconds_from_midnight() as i64);
    Ok(duration.num_milliseconds() as u32)
}

pub fn duration_ms_to_hms(ms: u32) -> DotResult<String> {
    let Some(native_date_time) = NaiveDateTime::from_timestamp_millis(ms as i64) else {
        return Err(DotError::FailParseTime);
    };
    let dt = DateTime::<Utc>::from_naive_utc_and_offset(native_date_time, Utc);
    Ok(match dt.hour() {
        0 => dt.format("%M:%S").to_string(),
        _ => dt.format("%H:%M:%S").to_string(),
    })
}
