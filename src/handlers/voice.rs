use crate::connection::get_voice_channel_for_user;
use crate::database::redis::REDIS;
use crate::handlers::music_panel::refresh_panel;
use crate::handlers::player::{Player, PLAYLIST_FETCHER_HANDLES};
use crate::result::errors::DotError;
use crate::result::DotResult;

use poise::serenity_prelude;
use serenity::model::guild::Guild;
use serenity::model::id::{ChannelId, UserId};
use serenity::model::voice::VoiceState;
use serenity::prelude::Mentionable;
use songbird::Call;
use std::sync::Arc;
use tokio::sync::Mutex;
use tokio::task;

pub async fn connect(
    serenity_ctx: &serenity_prelude::Context,
    guild: Guild,
    author: UserId,
) -> DotResult<Arc<Mutex<Call>>> {
    let manager = songbird::get(serenity_ctx).await.unwrap();

    let channel_opt = get_voice_channel_for_user(&guild, &author);
    let Some(channel_id) = channel_opt else {
        return Err(DotError::AuthorNotFound);
    };

    if let Some(call) = manager.get(guild.id) {
        let handler = call.lock().await;
        let has_current_connection = handler.current_connection().is_some();

        if has_current_connection {
            // bot is in another channel
            let bot_channel_id: ChannelId = handler.current_channel().unwrap().0.into();
            return Err(DotError::AlreadyConnected(bot_channel_id.mention()));
        }
    }

    let Ok(call) = manager.join(guild.id, channel_id).await else {
        return Err(DotError::CannotJoinChannel);
    };

    Ok(call)
}

pub async fn disconnect_handler(
    ctx: &serenity_prelude::Context,
    old: &Option<VoiceState>,
    new: &VoiceState,
) -> DotResult<()> {
    if new.member.as_ref().map(|m| m.user.id) != Some(ctx.cache.current_user().id) {
        return Ok(());
    }
    let Some(old) = old.as_ref() else {
        return Ok(());
    };
    if old.channel_id.is_some() && new.channel_id.is_none() {
        let Some(guild_id) = new.guild_id else {
            return Ok(());
        };
        let Some(player) = Player::get(guild_id) else {
            return Err(DotError::Other("Player not found"));
        };

        let ctx = ctx.clone();
        task::spawn(async move {
            // Stops every threads that are still fetching playlist items for the queue
            if let Some(handles) = PLAYLIST_FETCHER_HANDLES.get(&guild_id) {
                let handles = handles.value().lock().await;
                for handle in handles.iter() {
                    handle.abort();
                }
            }
            // clear the queue
            let mut q = REDIS.queue_ctl().await;
            q.clear(&guild_id.to_string()).await.unwrap();
            // refresh the music panel
            refresh_panel(&ctx, Some(guild_id)).await;
        });

        task::spawn(async move {
            player.remove().unwrap();
        });
    }
    Ok(())
}
