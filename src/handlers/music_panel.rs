use ::serenity::all::ComponentInteraction;
use ::serenity::builder::{
    CreateActionRow, CreateEmbedFooter, CreateInteractionResponse,
    CreateInteractionResponseMessage, EditMessage,
};
use poise::serenity_prelude as serenity;
use serde::{Deserialize, Serialize};
use serenity::all::CreateMessage;
use serenity::builder::{CreateButton, CreateEmbed, CreateEmbedAuthor};
use serenity::http::Http;
use serenity::model::channel::Message;
use serenity::model::id::{ChannelId, GuildId, MessageId};
use serenity::model::prelude::ReactionType::Unicode;
use serenity::ButtonStyle;
use serenity::Colour;
use std::sync::atomic::Ordering::Relaxed;
use std::sync::Arc;
use std::time::Duration;

use crate::client::BOT_AVATAR;
use crate::database::redis::REDIS;
use crate::handlers::player::extract::extract;
use crate::handlers::player::{Mode, Player, RepeatMode};
use crate::messages::now_playing::now_playing;
use crate::messages::queue::build_queue_page;

use crate::result::ok::DotMessages;
use crate::result::DotResult;

#[derive(Serialize, Deserialize)]
pub struct MusicPanel {
    pub channel_id: ChannelId,
    pub message: MessageId,
}

pub async fn interaction_response(http: &Arc<Http>, interaction: ComponentInteraction) {
    interaction
        .create_response(
            http,
            CreateInteractionResponse::UpdateMessage(CreateInteractionResponseMessage::default()),
        )
        .await
        .ok();
}

pub async fn panel_interaction_handler(
    ctx: &serenity::Context,
    interaction: ComponentInteraction,
) -> DotResult<()> {
    if let Err(e) = _panel_interaction_handler(ctx, interaction.clone()).await {
        let Some(panel) = REDIS
            .json_get::<MusicPanel>(&interaction.guild_id.unwrap().to_string(), "$.music_panel")
            .await
        else {
            return Ok(());
        };
        panel
            .channel_id
            .send_message(
                &ctx.http,
                CreateMessage::new().add_embed(CreateEmbed::new().description(e)),
            )
            .await
            .ok();
    }
    refresh_panel(ctx, interaction.guild_id).await;
    Ok(())
}

pub async fn _panel_interaction_handler(
    ctx: &serenity::Context,
    interaction: ComponentInteraction,
) -> DotResult<()> {
    let button_type = match interaction.data.custom_id.as_str() {
        "Pause" => ButtonType::PlayPause(true),
        "Resume" => ButtonType::PlayPause(false),
        "Skip" => ButtonType::Skip,
        "Stop" => ButtonType::Stop,
        "Shuffle" => ButtonType::Shuffle,
        "Leave" => ButtonType::Leave,
        "Station" => ButtonType::Station(false),
        "Station ON" => ButtonType::Station(true),
        "Repeat" => ButtonType::Repeat(RepeatMode::None),
        "Repeat ONE" => ButtonType::Repeat(RepeatMode::One),
        "Repeat ALL" => ButtonType::Repeat(RepeatMode::All),
        _ => return Ok(()),
    };

    let Some(player) = Player::get(interaction.guild_id.unwrap()) else {
        return Ok(());
    };

    match button_type {
        ButtonType::Skip => player.skip(None).await?,
        ButtonType::Stop => player.stop().await?,
        ButtonType::PlayPause(playing) => {
            if playing {
                player.pause().await?;
            } else {
                player.resume().await?;
            }
        }
        ButtonType::Repeat(_) => {
            player.repeat(None);
        }
        ButtonType::Shuffle => {
            REDIS
                .shuffle(&interaction.guild_id.unwrap().to_string())
                .await?
        }
        ButtonType::Leave => player.terminate().await?,
        ButtonType::Station(on) => {
            if on {
                player.station.store(false, Relaxed);
            } else {
                player.station.store(true, Relaxed);
                player.get_recommendations()?;
            }
        }
    }

    interaction_response(&ctx.http, interaction).await;

    Ok(())
}

pub async fn panel_receive_message_handler(
    ctx: &serenity::Context,
    msg: &Message,
) -> DotResult<()> {
    if let Err(e) = panel_receive_message(ctx, msg).await {
        let Some(panel) = REDIS
            .json_get::<MusicPanel>(&msg.guild_id.unwrap().to_string(), "$.music_panel")
            .await
        else {
            return Ok(());
        };
        panel
            .channel_id
            .send_message(
                &ctx.http,
                CreateMessage::new().add_embed(CreateEmbed::new().description(e)),
            )
            .await
            .ok();
    }

    Ok(())
}

pub async fn panel_receive_message(ctx: &serenity::Context, msg: &Message) -> DotResult<()> {
    let Some(guild_id) = msg.guild_id else {
        return Ok(());
    };

    let Some(music_panel) = REDIS
        .json_get::<MusicPanel>(&guild_id.to_string(), "$.music_panel")
        .await
    else {
        return Ok(());
    };
    if msg.channel_id != music_panel.channel_id {
        return Ok(());
    }
    if msg.author.id == ctx.cache.current_user().id {
        tokio::time::sleep(Duration::from_secs(3)).await;
        msg.delete(ctx).await.ok();
        return Ok(());
    }

    msg.delete(ctx).await.ok();

    let tracks = extract(msg.content.clone(), (msg.author.id, guild_id), None, None).await?;

    if let Some(player) = Player::get(guild_id) {
        player.play(tracks, Mode::End).await?;
    } else {
        let guild = msg.guild(&ctx.cache).unwrap().clone();
        Player::_new(ctx, guild, msg.author.id).await?.save();
        let player = Player::get(guild_id).unwrap();
        player.play(tracks, Mode::End).await?;
    }

    refresh_panel(ctx, msg.guild_id).await;

    Ok(())
}

pub async fn get_music_panel(guild_id: &GuildId) -> Option<MusicPanel> {
    REDIS
        .json_get::<MusicPanel>(&guild_id.to_string(), "$.music_panel")
        .await
}

pub fn fresh_panel() -> Vec<CreateEmbed> {
    let player_embed = CreateEmbed::default()
        .author(CreateEmbedAuthor::new(DotMessages::MusicPlayer))
        .title("Nothing is playing")
        .description("Enter a link or keywords to play.")
        .thumbnail(BOT_AVATAR)
        .color(Colour::from_rgb(88, 101, 242))
        .footer(CreateEmbedFooter::new("Dot").icon_url(BOT_AVATAR));

    vec![player_embed]
}

pub async fn refresh_panel(ctx: &serenity::Context, guild_id: Option<GuildId>) {
    let Some(guild_id) = guild_id else { return };
    let Some(panel) = get_music_panel(&guild_id).await else {
        return;
    };
    let Ok(mut message) = panel.channel_id.message(&ctx.http, panel.message).await else {
        return;
    };

    let player = Player::get(guild_id);
    let track = if player.is_some() {
        player.clone().unwrap().track.load_full()
    } else {
        None
    };

    let repeat = player
        .as_ref()
        .map(|p| *p.repeat_mode.load_full())
        .unwrap_or(RepeatMode::None);
    let playing = player
        .as_ref()
        .map(|p| p.playing.load(Relaxed))
        .unwrap_or(false);
    let station_state = player
        .as_ref()
        .map(|p| p.station.load(Relaxed))
        .unwrap_or(false);

    let setup_panel_components = |disabled: bool| {
        vec![
            CreateActionRow::Buttons(vec![
                ButtonType::PlayPause(playing).build(disabled),
                ButtonType::Skip.build(disabled),
                ButtonType::Shuffle.build(disabled),
                ButtonType::Repeat(repeat).build(disabled),
                ButtonType::Station(station_state).build(disabled),
            ]),
            CreateActionRow::Buttons(vec![
                ButtonType::Stop.build(disabled),
                ButtonType::Leave.build(disabled),
            ]),
        ]
    };

    match track {
        Some(_) => {
            let player_embed =
                now_playing(player.unwrap(), Some(DotMessages::MusicPlayer), true, true).unwrap();
            let embeds = if let Ok(queue_embed) = build_queue_page(guild_id, 0, false).await {
                vec![queue_embed.embed.unwrap(), player_embed]
            } else {
                vec![player_embed]
            };
            message
                .edit(
                    &ctx.http,
                    EditMessage::new()
                        .embeds(embeds)
                        .components(setup_panel_components(false)),
                )
                .await
                .ok();
        }
        None => {
            message
                .edit(
                    &ctx.http,
                    EditMessage::new()
                        .embeds(fresh_panel())
                        .components(setup_panel_components(true)),
                )
                .await
                .ok();
        }
    }
}

enum ButtonType {
    PlayPause(bool),
    Skip,
    Stop,
    Shuffle,
    Repeat(RepeatMode),
    Leave,
    Station(bool),
}

impl ButtonType {
    fn label(&self) -> &str {
        match self {
            ButtonType::Skip => "Skip",
            ButtonType::Stop => "Stop",
            ButtonType::Shuffle => "Shuffle",
            ButtonType::Leave => "Leave",
            ButtonType::PlayPause(playing) => match playing {
                true => "Pause",
                false => "Resume",
            },
            ButtonType::Repeat(mode) => match mode {
                RepeatMode::None => "Repeat",
                RepeatMode::One => "Repeat ONE",
                RepeatMode::All => "Repeat ALL",
            },
            ButtonType::Station(state) => match state {
                true => "Station ON",
                false => "Station",
            },
        }
    }

    fn style(&self) -> ButtonStyle {
        match self {
            ButtonType::Skip => ButtonStyle::Primary,
            ButtonType::Stop => ButtonStyle::Danger,
            ButtonType::Shuffle => ButtonStyle::Success,
            ButtonType::Leave => ButtonStyle::Danger,
            ButtonType::PlayPause(playing) => match playing {
                true => ButtonStyle::Secondary,
                false => ButtonStyle::Primary,
            },
            ButtonType::Repeat(mode) => match mode {
                RepeatMode::None => ButtonStyle::Success,
                _ => ButtonStyle::Secondary,
            },
            ButtonType::Station(state) => match state {
                true => ButtonStyle::Secondary,
                false => ButtonStyle::Success,
            },
        }
    }

    fn emoji(&self) -> &str {
        match self {
            ButtonType::Skip => "⏭️",
            ButtonType::Stop => "⏹️",
            ButtonType::Shuffle => "🔀",
            ButtonType::Repeat(_) => "🔁",
            ButtonType::Leave => "⏏️",
            ButtonType::Station(_) => "📡",
            ButtonType::PlayPause(playing) => match playing {
                true => "⏸️",
                false => "▶️",
            },
        }
    }

    fn build(&self, disabled: bool) -> CreateButton {
        CreateButton::new(self.label())
            .label(self.label())
            .style(self.style())
            .emoji(Unicode(self.emoji().to_string()))
            .disabled(disabled)
            .to_owned()
    }
}
