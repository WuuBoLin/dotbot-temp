pub mod music_panel;
pub mod player;
pub mod tools;
pub mod voice;

use crate::client::Data;

use crate::handlers::music_panel::{panel_interaction_handler, panel_receive_message_handler};
use crate::handlers::tools::{taobao, twitter};
use crate::handlers::voice::disconnect_handler;
use crate::result::errors::DotError;
use crate::result::DotResult;
use poise::{serenity_prelude as serenity, CreateReply};
use poise::{FrameworkContext, FrameworkError};
use serenity::all::{CreateEmbed, FullEvent};

pub async fn event_handler<'a, U, E>(
    event: &FullEvent,
    _framework: FrameworkContext<'a, U, E>,
) -> DotResult<()> {
    match event {
        FullEvent::VoiceStateUpdate { ctx, old, new } => {
            disconnect_handler(ctx, old, new).await?;
        }
        FullEvent::Message { ctx, new_message } => {
            twitter::fix_handler(ctx, new_message).await?;
            taobao::fix_handler(ctx, new_message).await?;
            panel_receive_message_handler(ctx, new_message).await?;
        }
        FullEvent::InteractionCreate { ctx, interaction } => {
            if let Some(message_component) = interaction.clone().message_component() {
                panel_interaction_handler(ctx, message_component).await?;
            }
        }
        _ => {}
    }
    Ok(())
}

pub async fn error_handler(error: FrameworkError<'_, Data, DotError>) {
    match error {
        FrameworkError::Command { error, ctx, .. } => {
            ctx.send(
                CreateReply::new()
                    .ephemeral(true)
                    .embed(CreateEmbed::new().description(error)),
            )
            .await
            .unwrap();
        }
        _ => {}
    }
}
