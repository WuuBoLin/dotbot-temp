use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering::Relaxed;
use std::sync::Arc;
use std::time::Duration;

use arc_swap::{ArcSwap, ArcSwapOption};
use chrono::{DateTime, Utc};
use dashmap::DashMap;
use enum_display::EnumDisplay;
use librespot::core::spotify_id::SpotifyId;

use once_cell::sync::Lazy;
use poise::serenity_prelude as serenity;
use poise::ChoiceParameter;
use rand::seq::SliceRandom;
use rand::thread_rng;

use serde::{Deserialize, Serialize};

use serenity::all::{CreateEmbed, CreateMessage};

use serenity::model::guild::Guild;
use serenity::model::id;
use serenity::model::id::{GuildId, UserId};
use songbird::input::{Input, RawAdapter, YoutubeDl};
use songbird::tracks::TrackHandle;
use songbird::{Call, Event, TrackEvent};

use tokio::sync::Mutex;
use tokio::task;
use tokio::task::AbortHandle;
use tracing::error;

use crate::client::Context;

use crate::database::redis::REDIS;
use crate::handlers::music_panel::refresh_panel;
use crate::handlers::player::queue::queue;
use crate::handlers::player::sources::spotify::{Spotify, SPOTIFY_SESSION};
use crate::handlers::player::sources::youtube::YouTube;

use crate::handlers::player::track_end_handler::TrackEndHandler;
use crate::handlers::voice;
use crate::messages::queue::EMBED_PAGE_SIZE;
use crate::result::errors::DotError;
use crate::result::DotResult;
use crate::time::duration_hms_to_ms;

pub mod extract;
pub mod queue;
pub mod sources;
mod track_end_handler;

#[derive(Clone, Copy, Debug, Serialize, Deserialize, ChoiceParameter, EnumDisplay)]
pub enum Source {
    YouTube,
    Spotify,
    Ambiguous,
}

#[derive(Clone, Copy, Debug, ChoiceParameter, EnumDisplay)]
pub enum Mode {
    End,
    Next,
    All,
    Shuffle,
    Jump,
}

#[derive(Clone, Copy, Debug, ChoiceParameter, EnumDisplay)]
pub enum RepeatMode {
    None,
    One,
    All,
}

#[derive(Clone)]
pub enum QueryType {
    Keywords(String),
    KeywordList(Vec<String>),
    VideoLink(String),
    PlaylistLink(String),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Track {
    pub id: String,
    pub title: String,
    pub source: Source,
    pub artists: Vec<String>,
    pub requested_by: UserId,
    pub is_live: bool,
    pub duration_ms: u32,
    pub thumbnail_url: Option<String>,
    pub start_time: Option<DateTime<Utc>>,
    pub start_from: u32,
}

pub struct Player {
    pub guild_id: GuildId,
    pub call: Arc<Mutex<Call>>,
    pub spotify: ArcSwap<Spotify>,
    pub playing: AtomicBool,
    pub station: AtomicBool,
    pub repeat_mode: ArcSwap<RepeatMode>,
    pub track: ArcSwapOption<Track>,
    pub source: ArcSwapOption<Source>,
    pub track_handle: ArcSwapOption<TrackHandle>,
}

pub static PLAYERS: Lazy<DashMap<GuildId, Arc<Player>>> = Lazy::new(DashMap::new);
pub static SERENITY_CONTEXTS: Lazy<DashMap<GuildId, serenity::client::Context>> =
    Lazy::new(DashMap::new);
pub static PLAYLIST_FETCHER_HANDLES: Lazy<DashMap<GuildId, Arc<Mutex<Vec<AbortHandle>>>>> =
    Lazy::new(DashMap::new);

pub static REQWEST_CLIENT: Lazy<reqwest::Client> = Lazy::new(reqwest::Client::new);

impl Player {
    pub async fn new(ctx: Context<'_>) -> DotResult<Self> {
        // Stops every threads that are still fetching playlist items for the queue
        if let Some(handles) = PLAYLIST_FETCHER_HANDLES.get(&ctx.guild_id().unwrap()) {
            let handles = handles.value().lock().await;
            for handle in handles.iter() {
                handle.abort();
            }
        }

        // clear the queue
        let mut q = REDIS.queue_ctl().await;
        if (q.clear(&ctx.guild_id().unwrap().to_string()).await).is_err() {
            return Err(DotError::Other("Failed to clear queue"));
        }

        let guild = ctx.guild().ok_or(DotError::NoServerInformation)?.to_owned();

        Self::_new(ctx.serenity_context(), guild, ctx.author().id).await
    }

    pub async fn _new(
        serenity_ctx: &serenity::Context,
        guild: Guild,
        author: UserId,
    ) -> DotResult<Self> {
        let guild_id = guild.id;
        let call = voice::connect(serenity_ctx, guild, author).await?;

        SERENITY_CONTEXTS.insert(guild_id, serenity_ctx.clone());

        Ok(Self {
            guild_id,
            call,
            track: ArcSwapOption::default(),
            source: ArcSwapOption::default(),
            track_handle: ArcSwapOption::default(),
            spotify: ArcSwap::new(Arc::new(Spotify::new(None)?)),
            playing: AtomicBool::new(false),
            station: AtomicBool::new(false),
            repeat_mode: ArcSwap::new(Arc::new(RepeatMode::None)),
        })
    }

    pub fn save(self) {
        PLAYERS.insert(self.guild_id, Arc::new(self));
    }

    pub fn get(guild_id: GuildId) -> Option<Arc<Player>> {
        if let Some(player) = PLAYERS.get(&guild_id) {
            return Some(player.clone());
        }

        None
    }

    pub fn remove(&self) -> DotResult<()> {
        if PLAYERS.remove(&self.guild_id).is_none() {
            return Err(DotError::Other("Player not found"));
        }

        Ok(())
    }

    pub fn now_playing(&self) -> Option<Arc<Track>> {
        self.track.load_full()
    }

    pub async fn spotify_stop(&self) {
        if self.source.load().is_some() {
            if let Source::Spotify = **self.source.load().as_ref().unwrap() {
                self.spotify.load().player.stop();
                tokio::time::sleep(Duration::from_millis(100)).await;
            }
        }
    }

    pub async fn stop(&self) -> DotResult<()> {
        if let Some(track_handle) = self.track_handle.load_full() {
            track_handle.stop().ok();
        }

        // reset the player
        self.source.store(None);
        self.track.store(None);
        self.track_handle.store(None);
        self.playing.store(false, Relaxed);

        // stop the player
        self.call.lock().await.stop();

        // Stops every threads that are still fetching playlist items for the queue
        if let Some(handles) = PLAYLIST_FETCHER_HANDLES.get(&self.guild_id) {
            let handles = handles.value().lock().await;
            for handle in handles.iter() {
                handle.abort();
            }
        }

        // clear the queue
        let mut q = REDIS.queue_ctl().await;
        if (q.clear(&self.guild_id.to_string()).await).is_err() {
            return Err(DotError::Other("Failed to clear queue"));
        }

        Ok(())
    }

    pub async fn terminate(&self) -> DotResult<()> {
        self.call.lock().await.remove_all_global_events();

        if self.playing.load(Relaxed) {
            self.spotify_stop().await;
        } else if self.track.load().is_some() {
            self.resume().await?;
            self.spotify_stop().await;
        }

        // stop the player
        self.stop().await?;

        // leave the voice channel
        self.call
            .lock()
            .await
            .leave()
            .await
            .map_err(|_| DotError::Other("Unable to leave voice channel"))?;

        // finally remove the player
        self.remove()?;

        Ok(())
    }

    pub async fn skip(&self, index: Option<u64>) -> DotResult<()> {
        if index.map(|x| x == 0).unwrap_or(false) {
            return Err(DotError::FailRemoveTrack);
        }

        let previous_track = self.track.load_full();

        let mut q = REDIS.queue_ctl().await;
        let track = match index {
            None => q.first_track(&self.guild_id.to_string()).await,
            Some(index) => {
                if index > q.length(&self.guild_id.to_string()).await {
                    return Err(DotError::FailRemoveTrack);
                }
                q.get_track(&self.guild_id.to_string(), index - 1).await
            }
        };

        let Some(track) = track else {
            self.stop().await?;
            return Ok(());
        };

        self._play(track).await?;

        if matches!(**self.repeat_mode.load(), RepeatMode::All) {
            if let Some(previous_track) = previous_track {
                q.append(&self.guild_id.to_string(), &[(*previous_track).clone()])
                    .await
                    .unwrap();
            }
        }

        if q.trim_from(&self.guild_id.to_string(), index.unwrap_or(1))
            .await
            .is_err()
        {
            return Err(DotError::Other("Failed to trim queue"));
        }

        Ok(())
    }

    pub async fn seek(&self, position_ms: u32) -> DotResult<()> {
        if self.track.load().is_none() {
            return Err(DotError::NothingPlaying);
        }

        // modify the track start time
        let mut track = (*self.track.load_full().unwrap()).clone();
        track.start_from = position_ms;
        track.start_time = Some(Utc::now());
        self.track.store(Some(Arc::new(track)));

        match *self.source.load().clone().unwrap() {
            Source::Spotify => self.spotify.load().player.seek(position_ms),
            Source::YouTube | Source::Ambiguous => {
                self.track_handle
                    .load_full()
                    .unwrap()
                    .seek(Duration::from_millis(position_ms as u64))
                    .result_async()
                    .await
                    .map_err(|_| DotError::Other("Failed to seek"))?;
            }
        }
        Ok(())
    }

    pub async fn pause(&self) -> DotResult<()> {
        if let Some(track_handle) = self.track_handle.load_full() {
            track_handle
                .pause()
                .map_err(|_| DotError::FailPlayer("pause"))?;
        }

        if self.track.load().is_none() {
            return Err(DotError::NothingPlaying);
        }

        if matches!(*self.source.load().clone().unwrap(), Source::Spotify) {
            self.spotify.load().player.pause();
        }

        let mut track = (*self.track.load_full().unwrap()).clone();
        track.start_from = duration_hms_to_ms(self.progress()?)?;
        track.start_time = Some(Utc::now());
        self.track.store(Some(Arc::new(track)));

        self.playing.store(false, Relaxed);
        Ok(())
    }

    pub async fn resume(&self) -> DotResult<()> {
        if let Some(track_handle) = self.track_handle.load_full() {
            track_handle
                .play()
                .map_err(|_| DotError::FailPlayer("resume"))?;
        }

        if self.track.load().is_none() {
            return Err(DotError::NothingPlaying);
        }

        if matches!(*self.source.load().clone().unwrap(), Source::Spotify) {
            self.spotify.load().player.play();
        }

        let mut track = (*self.track.load_full().unwrap()).clone();
        track.start_time = Some(Utc::now());
        self.track.store(Some(Arc::new(track)));

        self.playing.store(true, Relaxed);
        Ok(())
    }

    pub fn repeat(&self, mode: Option<RepeatMode>) -> RepeatMode {
        if let Some(mode) = mode {
            self.repeat_mode.swap(Arc::from(mode));
            return *self.repeat_mode.load_full();
        }

        match *self.repeat_mode.load_full() {
            RepeatMode::None => self.repeat_mode.swap(Arc::from(RepeatMode::All)),
            RepeatMode::All => self.repeat_mode.swap(Arc::from(RepeatMode::One)),
            RepeatMode::One => self.repeat_mode.swap(Arc::from(RepeatMode::None)),
        };

        *self.repeat_mode.load_full()
    }

    pub fn progress(&self) -> DotResult<String> {
        let Some(track) = self.track.load_full() else {
            return Ok(String::from("00:00 / 00:00"));
        };

        let duration = Utc::now().signed_duration_since(track.start_time.unwrap())
            + chrono::Duration::milliseconds(track.start_from as i64);
        let hours = duration.num_hours();
        let minutes = duration.num_minutes() % 60;
        let seconds = duration.num_seconds() % 60;

        Ok(match hours {
            0 => format!("{:02}:{:02}", minutes, seconds),
            _ => format!("{:02}:{:02}:{:02}", hours, minutes, seconds),
        })
    }

    pub fn get_recommendations(&self) -> DotResult<()> {
        if !self.station.load(Relaxed) {
            return Ok(());
        }

        if !matches!(**self.repeat_mode.load(), RepeatMode::None) {
            self.station.store(false, Relaxed);
            return Err(DotError::StationConflictsRepeat);
        }

        if self.track.load().is_none() {
            return Ok(());
        };

        let Some(source) = self.source.load_full() else {
            return Ok(());
        };

        let guild_id = self.guild_id;
        let now_track = self.track.load_full().map(|t| (*t).clone()).unwrap();
        let ctx = SERENITY_CONTEXTS.get(&guild_id).unwrap();
        task::spawn(async move {
            let mut q = REDIS.queue_ctl().await;
            let queue_len = q.length(&guild_id.to_string()).await;

            if queue_len >= EMBED_PAGE_SIZE {
                return;
            }

            let result = match *source {
                Source::YouTube => YouTube::recommend(guild_id, now_track, queue_len).await,
                Source::Spotify => {
                    let queue = match q.get_tracks(&guild_id.to_string(), 0, queue_len).await {
                        None => vec![now_track],
                        Some(mut queue) => {
                            queue.append(&mut vec![now_track]);
                            queue
                        }
                    };

                    Spotify::recommend(guild_id, queue, queue_len).await
                }
                Source::Ambiguous => Ok(()),
            };

            if let Err(err) = result {
                error!("{}", err);
            }

            refresh_panel(&ctx, Some(guild_id)).await;
        });

        Ok(())
    }

    pub async fn play_next(&self, mode: RepeatMode) {
        let previous_track = self.track.load_full();

        let mut q = REDIS.queue_ctl().await;
        let Some(first_track) = q.first_track(&self.guild_id.to_string()).await else {
            self.track.store(None);
            self.source.store(None);
            self.track_handle.store(None);
            return;
        };

        if let Err(error) = self._play(first_track).await {
            if let Some(music_panel) = REDIS
                .json_get::<id::ChannelId>(&self.guild_id.to_string(), "$.music_panel.channel_id")
                .await
            {
                if let Some(ctx) = SERENITY_CONTEXTS.get(&self.guild_id) {
                    music_panel
                        .send_message(
                            &ctx.http,
                            CreateMessage::new().embed(CreateEmbed::new().description(error)),
                        )
                        .await
                        .ok();
                }
            }
        }

        q.pop(&self.guild_id.to_string()).await.unwrap();

        if matches!(mode, RepeatMode::All) {
            if let Some(previous_track) = previous_track {
                q.append(&self.guild_id.to_string(), &[(*previous_track).clone()])
                    .await
                    .unwrap();
            }
        }
    }

    pub async fn play_again(&self) {
        let track = &*self.track.load().clone().unwrap();
        if let Err(error) = self._play(track.clone()).await {
            if let Some(music_panel) = REDIS
                .json_get::<id::ChannelId>(&self.guild_id.to_string(), "$.music_panel.channel_id")
                .await
            {
                if let Some(ctx) = SERENITY_CONTEXTS.get(&self.guild_id) {
                    music_panel
                        .send_message(
                            &ctx.http,
                            CreateMessage::new().embed(CreateEmbed::new().description(error)),
                        )
                        .await
                        .ok();
                }
            }
        }
    }

    pub async fn play(&self, mut tracks: Vec<Track>, mode: Mode) -> DotResult<()> {
        // check if the vector is empty
        if tracks.is_empty() {
            return Err(DotError::TrackFail);
        }

        // if there is a track playing, queue the tracks
        if self.track.load().is_some() {
            // if the mode is "Jump", play it now and then queue the tracks
            if matches!(mode, Mode::Jump) {
                self._play(tracks.remove(0)).await?;
                if !tracks.is_empty() {
                    return queue(tracks, self.guild_id, mode).await;
                }
                return Ok(());
            }

            return queue(tracks, self.guild_id, mode).await;
        }

        // if the mode is "Shuffle", shuffle the tracks first
        if matches!(mode, Mode::Shuffle) {
            tracks.shuffle(&mut thread_rng());
        }

        // take the first track from the queue and check if it is the last track
        let track = tracks.remove(0);
        if !tracks.is_empty() {
            queue(tracks, self.guild_id, mode).await?;
        }

        // play the track
        self._play(track).await?;

        Ok(())
    }

    /// The real "Play" function to directly plays a track
    pub async fn _play(&self, mut track: Track) -> DotResult<()> {
        // get the the voice connection
        let mut call = self.call.lock().await;

        // start the internal player for the specific source and get the output sink
        let input = match track.source {
            Source::YouTube => self._play_ytdl(&track).await,
            Source::Spotify => self._play_spotify(&track).await,
            Source::Ambiguous => self._play_ytdl(&track).await,
        };

        // play it to Discord
        let track_handle = call.play_only_input(input);
        track_handle.play().unwrap();

        call.remove_all_global_events();
        call.add_global_event(
            Event::Track(TrackEvent::End),
            TrackEndHandler {
                guild_id: self.guild_id,
                ctx: SERENITY_CONTEXTS.get(&self.guild_id).unwrap().clone(),
            },
        );

        drop(call);

        // modify the track start time
        track.start_time = Some(Utc::now());

        // store the track and the track handle
        self.track.store(Some(Arc::new(track)));
        self.track_handle.store(Some(Arc::new(track_handle)));
        self.playing.store(true, Relaxed);

        self.get_recommendations()?;

        Ok(())
    }

    async fn _play_ytdl(&self, track: &Track) -> Input {
        let youtube_dl = YoutubeDl::new(REQWEST_CLIENT.to_owned(), track.id.clone());

        self.source.store(Some(Arc::new(track.source)));

        Input::from(youtube_dl)
    }

    async fn _play_spotify(&self, track: &Track) -> Input {
        let spotify_id = SpotifyId::from_uri(&track.id).unwrap();

        while SPOTIFY_SESSION
            .read()
            .unwrap()
            .as_ref()
            .unwrap()
            .is_invalid()
        {
            let spotify = self.spotify.load();
            Spotify::login().await.unwrap();
            self.spotify
                .store(Arc::new(Spotify::new(Some(spotify.sink.clone())).unwrap()))
        }

        let spotify = self.spotify.load();
        let player = spotify.player.clone();
        let sink = spotify.sink.clone();
        drop(spotify);

        // PLAY!!!
        player.load(spotify_id, true, 0);

        self.source.store(Some(Arc::new(Source::Spotify)));

        Input::from(RawAdapter::new(sink, librespot::playback::SAMPLE_RATE, 2))
    }
}
