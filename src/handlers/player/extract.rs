use crate::handlers::player::sources::spotify::{Spotify, SpotifyApi, SPOTIFY_URI_REGEX};

use crate::handlers::player::Track;
use crate::handlers::player::{Mode, Source};

use crate::result::DotResult;
use serenity::model::id::{GuildId, UserId};
use url::Url;

use crate::database::redis::REDIS;
use crate::handlers::player::sources::youtube::YouTube;
use crate::result::errors::DotError;

use super::sources::ambiguous::Ambiguous;

pub async fn extract(
    query: String,
    requested_by: (UserId, GuildId),
    mode: Option<Mode>,
    source: Option<Source>,
) -> DotResult<Vec<Track>> {
    if SPOTIFY_URI_REGEX.captures(&query).is_some() {
        return Spotify::extract(&query, requested_by, mode).await;
    }

    match Url::parse(&query) {
        Ok(url) => match url.host_str() {
            Some("open.spotify.com") => Spotify::extract(&query, requested_by, mode).await,
            Some("www.youtube.com")
            | Some("youtu.be")
            | Some("youtube.com")
            | Some("m.youtube.com") => YouTube::extract(&query, requested_by, mode).await,
            Some(_link) => Ambiguous::extract(&query, requested_by).await,
            None => Err(DotError::TrackFail),
        },
        Err(_) => search(query, requested_by, source, true).await,
    }
}

async fn search(
    query: String,
    requested_by: (UserId, GuildId),
    source: Option<Source>,
    get_first: bool,
) -> DotResult<Vec<Track>> {
    let guild_id = requested_by.1;

    let search_source = match source {
        Some(Source::Spotify) => Source::Spotify,
        Some(Source::YouTube) => Source::YouTube,
        Some(Source::Ambiguous) => Source::Ambiguous,
        None => match REDIS
            .json_get::<Source>(&guild_id.to_string(), "$.default_search")
            .await
        {
            Some(default_search) => default_search,
            None => {
                REDIS
                    .json_set(&guild_id.to_string(), "$.default_search", &Source::Spotify)
                    .await
                    .unwrap();
                Source::Spotify
            }
        },
    };

    let limit = if get_first { 1 } else { 50 };

    match search_source {
        Source::YouTube => YouTube::search(&query, limit, requested_by.0).await,
        Source::Spotify => SpotifyApi::search(&query, limit, requested_by.0).await,
        Source::Ambiguous => Err(DotError::WrongSearchSource(Source::Ambiguous)),
    }
}
