use async_trait::async_trait;

use crate::handlers::player::{Player, RepeatMode};

use serenity::model::id::GuildId;
use songbird::tracks::PlayMode;
use songbird::EventHandler as VoiceEventHandler;
use songbird::{Event, EventContext, TrackEvent};

use crate::handlers::music_panel::refresh_panel;

pub struct TrackEndHandler {
    pub guild_id: GuildId,
    pub ctx: serenity::client::Context,
}

#[async_trait]
impl VoiceEventHandler for TrackEndHandler {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        if let EventContext::Track([(state, _)]) = ctx {
            if !matches!(state.playing, PlayMode::End) {
                return None;
            }
        }

        let player = Player::get(self.guild_id).unwrap();
        match **player.repeat_mode.load() {
            RepeatMode::None => player.play_next(RepeatMode::None).await,
            RepeatMode::One => player.play_again().await,
            RepeatMode::All => player.play_next(RepeatMode::All).await,
        }

        refresh_panel(&self.ctx, Some(self.guild_id)).await;

        Some(Event::Track(TrackEvent::End))
    }
}
