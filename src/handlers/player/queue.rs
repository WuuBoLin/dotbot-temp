use crate::handlers::player::{Mode, Track};
use crate::result::errors::DotError;
use crate::result::DotResult;
use rand::prelude::SliceRandom;
use redis::Value::Bulk;

use crate::database::redis::REDIS;

use serenity::model::id::GuildId;

pub async fn queue(mut tracks: Vec<Track>, guild_id: GuildId, mode: Mode) -> DotResult<()> {
    let mut q = REDIS.queue_ctl().await;
    let guild_id = &guild_id.to_string();

    let tracks = match mode {
        Mode::Shuffle => {
            tracks.shuffle(&mut rand::thread_rng());
            tracks
        }
        _ => tracks,
    };

    let result = match mode {
        Mode::End => q.append(guild_id, &tracks).await,
        Mode::All => q.append(guild_id, &tracks).await,
        Mode::Shuffle => q.append(guild_id, &tracks).await,
        Mode::Next => q.insert(guild_id, &tracks).await,
        Mode::Jump => q.insert(guild_id, &tracks).await,
    };

    // If the return Ok(Bulk(values)) is not empty, then the queue already exists.
    if let Ok(Bulk(values)) = result {
        if !values.is_empty() {
            return Ok(());
        }
    }

    // If the above check fails, then the queue does not exist, so we create it.
    if REDIS
        .json_set::<Vec<Track>>(guild_id, "$.queue", &tracks)
        .await
        .is_err()
    {
        return Err(DotError::TrackFail);
    }

    Ok(())
}
