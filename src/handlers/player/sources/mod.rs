use crate::handlers::player::{Source, Track};
use std::sync::Arc;

pub mod ambiguous;
// pub mod ffmpeg;
pub mod spotify;
pub mod twitch;
pub mod youtube;

pub fn get_track_source_url(track: &Arc<Track>) -> String {
    match track.source {
        Source::YouTube => {
            format!("https://www.youtube.com/watch?v={}", track.id)
        }
        Source::Spotify => {
            format!(
                "https://open.spotify.com/track/{}",
                track.id.replace("spotify:track:", "")
            )
        }
        Source::Ambiguous => track.id.clone(),
    }
}
