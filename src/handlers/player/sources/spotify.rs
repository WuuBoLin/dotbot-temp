use crate::handlers::player::{Mode, Source, Track, PLAYLIST_FETCHER_HANDLES};
use crate::messages::SPOTIFY_INVALID_QUERY;
use crate::result::errors::DotError;
use crate::result::DotResult;
use byteorder::{ByteOrder, LittleEndian};
use librespot::core::config::SessionConfig;
use librespot::core::session::Session;

use librespot::discovery::Credentials;
use librespot::playback::audio_backend;
use librespot::playback::audio_backend::SinkResult;
use librespot::playback::convert::Converter;
use librespot::playback::decoder::AudioPacket;
use once_cell::sync::Lazy;
use regex::{Captures, Regex};

use serenity::model::id::{GuildId, UserId};
use songbird::input::core::io::MediaSource;

use std::str::FromStr;
use std::sync::mpsc::{sync_channel, Receiver, SyncSender};
use std::sync::{Arc, Mutex, RwLock};

use arc_swap::ArcSwapOption;
use std::borrow::Cow;

use std::{env, io};

use librespot::playback::config::{Bitrate, PlayerConfig};
use librespot::playback::player::{Player as SpotifyPlayer, PlayerEvent, PlayerEventChannel};
use rspotify::clients::BaseClient;
use rspotify::model::{
    AlbumId, ArtistId, FullTrack, PlayableItem, PlaylistId, SearchResult, SearchType, TrackId,
};
use rspotify::ClientCredsSpotify;

use crate::client::BOT_USERID;
use crate::database::redis::REDIS;
use crate::handlers::player::queue::queue;
use crate::messages::queue::EMBED_PAGE_SIZE;

use tokio::task;
use tracing::{error, warn};

pub static SPOTIFY_SESSION: RwLock<Option<Session>> = RwLock::new(None);
pub static SPOTIFY_URI_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"spotify:(?P<media_type>track|album|playlist):(?P<media_id>[a-zA-Z0-9]+)$").unwrap()
});
pub static SPOTIFY_QUERY_REGEX: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"spotify.com/(?P<media_type>.+)/(?P<media_id>.*?)(?:\?|$)").unwrap());

#[derive(Clone, Copy)]
pub enum MediaType {
    Track,
    Album,
    Playlist,
}

impl FromStr for MediaType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "track" => Ok(Self::Track),
            "album" => Ok(Self::Album),
            "playlist" => Ok(Self::Playlist),
            _ => Err(()),
        }
    }
}

pub struct Spotify {
    pub player: Arc<SpotifyPlayer>,
    pub sink: SpotifySink,
}

impl Spotify {
    pub async fn login() -> DotResult<()> {
        let username = env::var("SPOTIFY_USERNAME").expect("Fatality! SPOTIFY_USERNAME not set!");
        let password = env::var("SPOTIFY_PASSWORD").expect("Fatality! SPOTIFY_PASSWORD not set!");
        let session = Session::new(SessionConfig::default(), None); // TODO: add cache
        let credentials = Credentials::with_password(username, password);
        session.connect(credentials, false).await.unwrap();

        let mut spotify_session = SPOTIFY_SESSION.write().unwrap();
        *spotify_session = Some(session);

        Ok(())
    }

    pub fn new(sink: Option<SpotifySink>) -> DotResult<Self> {
        let sink = sink.unwrap_or_default();
        let cloned_sink = sink.clone();

        let player_config = PlayerConfig {
            bitrate: Bitrate::Bitrate320,
            gapless: true,
            ..Default::default()
        };

        let player = SpotifyPlayer::new(
            player_config,
            SPOTIFY_SESSION.read().unwrap().clone().unwrap(),
            Box::new(librespot::playback::mixer::NoOpVolume),
            move || Box::new(cloned_sink),
        );

        sink.event_channel.store(Some(Arc::new(Mutex::new(
            player.get_player_event_channel(),
        ))));

        Ok(Self { player, sink })
    }

    pub async fn extract(
        query: &str,
        requested_by: (UserId, GuildId),
        mode: Option<Mode>,
    ) -> DotResult<Vec<Track>> {
        let captures: Captures;
        if let Some(c) = SPOTIFY_URI_REGEX.captures(query) {
            captures = c;
        } else {
            captures = SPOTIFY_QUERY_REGEX
                .captures(query)
                .ok_or(DotError::Other(SPOTIFY_INVALID_QUERY))?;
        }

        let media_type = captures
            .name("media_type")
            .ok_or(DotError::Other(SPOTIFY_INVALID_QUERY))?
            .as_str();

        let media_type =
            MediaType::from_str(media_type).map_err(|_| DotError::Other(SPOTIFY_INVALID_QUERY))?;

        let media_id = captures
            .name("media_id")
            .ok_or(DotError::Other(SPOTIFY_INVALID_QUERY))?
            .as_str();

        match media_type {
            MediaType::Track => Self::extract_track(media_id, requested_by.0).await,
            MediaType::Album => Self::extract_album(media_id, requested_by.0).await,
            MediaType::Playlist => Self::extract_playlist(media_id, requested_by, mode).await,
        }
    }

    async fn extract_track(spotify_id: &str, requested_by: UserId) -> DotResult<Vec<Track>> {
        let client = SpotifyApi::auth().await?;
        let track = client
            .track(
                TrackId::from_id(spotify_id).map_err(|_| DotError::TrackFail)?,
                None,
            )
            .await
            .map_err(|_| DotError::TrackFail)?;

        Self::construct_track(track, requested_by)
    }

    async fn extract_album(spotify_id: &str, requested_by: UserId) -> DotResult<Vec<Track>> {
        let client = SpotifyApi::auth().await?;
        let album = client
            .album(
                AlbumId::from_id(spotify_id).map_err(|_| DotError::TrackFail)?,
                None,
            )
            .await
            .map_err(|_| DotError::TrackFail)?;

        let mut tracks = Vec::with_capacity(album.tracks.total as usize);
        for track in album.tracks.items {
            let Some(track_id) = track.id else {
                return Err(DotError::TrackFail);
            };
            tracks.push(Track {
                id: track_id.to_string(),
                title: track.name,
                source: Source::Spotify,
                artists: track
                    .artists
                    .iter()
                    .map(|artist| artist.name.clone())
                    .collect::<Vec<String>>(),
                requested_by,
                is_live: false,
                duration_ms: track.duration.num_milliseconds() as u32,
                thumbnail_url: Some(album.images.first().unwrap().clone().url),
                start_time: None,
                start_from: 0,
            })
        }

        Ok(tracks)
    }

    async fn extract_playlist(
        spotify_id: &str,
        requested_by: (UserId, GuildId),
        mode: Option<Mode>,
    ) -> DotResult<Vec<Track>> {
        let client = SpotifyApi::auth().await?;

        let playlist_id =
            PlaylistId::from_id(spotify_id.to_string()).map_err(|_| DotError::TrackFail)?;
        let playlist = client
            .playlist(playlist_id.clone(), Some("fields=tracks.total"), None)
            .await
            .map_err(|_| DotError::TrackFail)?;
        let playlist_len = playlist.tracks.total;

        let mut tracks = Vec::with_capacity(100);

        let limit = 50;
        let mut offset = 0;
        loop {
            let page = client
                .playlist_items_manual(
                    playlist_id.clone(),
                    Some("fields=tracks.items"),
                    None,
                    Some(limit),
                    Some(offset),
                )
                .await
                .unwrap();

            for item in page.items {
                if let Some(PlayableItem::Track(track)) = item.track {
                    let track = Self::construct_track(track, requested_by.0)?;
                    tracks.extend(track);
                }
            }

            if tracks.len() >= 100 && playlist_len > 100 {
                offset += limit;
                let mut other_tracks = Vec::new();
                let playlist_fetcher_handle = task::spawn(async move {
                    loop {
                        let page = client
                            .playlist_items_manual(
                                playlist_id.clone(),
                                Some("fields=tracks.items"),
                                None,
                                Some(limit),
                                Some(offset),
                            )
                            .await
                            .unwrap();

                        for item in page.items {
                            if let Some(PlayableItem::Track(track)) = item.track {
                                let track = Self::construct_track(track, requested_by.0).ok();
                                if let Some(track) = track {
                                    other_tracks.extend(track);
                                }
                            }
                        }

                        if page.next.is_none() {
                            break;
                        }

                        offset += limit;
                    }

                    let mode = mode.unwrap_or(Mode::End);

                    queue(other_tracks, requested_by.1, mode).await.ok();
                });

                if let Some(handles) = PLAYLIST_FETCHER_HANDLES.get(&requested_by.1) {
                    handles
                        .lock()
                        .await
                        .push(playlist_fetcher_handle.abort_handle());
                } else {
                    PLAYLIST_FETCHER_HANDLES.insert(
                        requested_by.1,
                        Arc::new(tokio::sync::Mutex::new(vec![
                            playlist_fetcher_handle.abort_handle()
                        ])),
                    );
                }

                return Ok(tracks);
            }

            if page.next.is_none() {
                break;
            }

            offset += limit;
        }

        Ok(tracks)
    }

    fn construct_track(track: FullTrack, requested_by: UserId) -> DotResult<Vec<Track>> {
        let Some(track_id) = track.id else {
            warn!("Track `{}` ID not found", track.name);
            return Err(DotError::TrackFail);
        };

        let thumbnail_url = track.album.images.first().map(|image| image.url.clone());

        Ok(vec![Track {
            id: track_id.to_string(),
            title: track.name,
            source: Source::Spotify,
            artists: track
                .artists
                .iter()
                .map(|artist| artist.name.clone())
                .collect::<Vec<String>>(),
            requested_by,
            is_live: false,
            duration_ms: track.duration.num_milliseconds() as u32,
            thumbnail_url,
            start_time: None,
            start_from: 0,
        }])
    }

    pub async fn recommend(guild_id: GuildId, tracks: Vec<Track>, queue_len: u64) -> DotResult<()> {
        let mut track_seeds = Vec::with_capacity(queue_len as usize);
        for track in tracks {
            let track_uri = Cow::from(track.id);
            let Some(track_id) = TrackId::from_uri(&track_uri).ok() else {
                continue;
            };
            track_seeds.push(track_id.clone_static());
        }

        if track_seeds.is_empty() {
            return Ok(());
        }

        let needed = EMBED_PAGE_SIZE - queue_len;

        let client = SpotifyApi::auth().await?;
        let seed_artists: Option<Vec<ArtistId>> = None;
        let seed_genres: Option<Vec<&str>> = None;
        let recommended_simplified_tracks = client
            .recommendations(
                vec![],
                seed_artists,
                seed_genres,
                Some(track_seeds),
                None,
                Some(needed as u32),
            )
            .await
            .map_err(|e| {
                error!("{}", e);
                DotError::ErrorRecommend
            })?
            .tracks;

        let mut recommended_tracks: Vec<Track> = Vec::with_capacity(needed as usize);
        for track in recommended_simplified_tracks {
            let track_id = track.id.as_ref().unwrap().clone_static();

            let recommended_track = Track {
                id: track_id.to_string(),
                title: track.name,
                source: Source::Spotify,
                artists: track.artists.iter().map(|a| a.name.clone()).collect(),
                requested_by: *BOT_USERID.get().unwrap(),
                is_live: false,
                duration_ms: track.duration.num_milliseconds() as u32,
                thumbnail_url: Some(
                    client
                        .track(track_id, None)
                        .await
                        .map_err(|e| {
                            error!("{}", e);
                            DotError::ErrorRecommend
                        })?
                        .album
                        .images
                        .first()
                        .unwrap()
                        .clone()
                        .url,
                ),
                start_time: None,
                start_from: 0,
            };

            recommended_tracks.push(recommended_track);
        }

        let mut q = REDIS.queue_ctl().await;
        q.append(&guild_id.to_string(), recommended_tracks.as_slice())
            .await
            .unwrap();

        Ok(())
    }
}

pub struct SpotifyApi;

impl SpotifyApi {
    pub async fn auth() -> DotResult<ClientCredsSpotify> {
        let spotify_client_id = env::var("SPOTIFY_CLIENT_ID")
            .map_err(|_| DotError::Other("missing spotify client ID"))?;

        let spotify_client_secret = env::var("SPOTIFY_CLIENT_SECRET")
            .map_err(|_| DotError::Other("missing spotify client secret"))?;

        let creds = rspotify::Credentials::new(&spotify_client_id, &spotify_client_secret);

        let client = ClientCredsSpotify::new(creds);
        client.request_token().await?;

        Ok(client)
    }

    pub async fn search(query: &str, limit: u32, requested_by: UserId) -> DotResult<Vec<Track>> {
        let client = Self::auth().await?;
        let Ok(search_result) = client
            .search(query, SearchType::Track, None, None, Some(limit), None)
            .await
        else {
            return Err(DotError::TrackFail);
        };

        let mut tracks: Vec<Track> = Vec::with_capacity(limit as usize);
        if let SearchResult::Tracks(page) = search_result {
            for track in page.items {
                let Some(track_id) = track.id else { continue };
                tracks.push(Track {
                    id: track_id.to_string(),
                    title: track.name,
                    source: Source::Spotify,
                    artists: track
                        .artists
                        .iter()
                        .map(|artist| artist.name.clone())
                        .collect::<Vec<String>>(),
                    requested_by,
                    is_live: false,
                    duration_ms: track.duration.num_milliseconds() as u32,
                    thumbnail_url: Some(track.album.images.first().unwrap().clone().url),
                    start_time: None,
                    start_from: 0,
                })
            }
        }

        Ok(tracks)
    }
}

// Spotify media sink for songbird
pub struct SpotifySink {
    event_channel: ArcSwapOption<Mutex<PlayerEventChannel>>,
    sender: Arc<SyncSender<[f32; 2]>>,
    pub receiver: Arc<Mutex<Receiver<[f32; 2]>>>,
    ended: bool,
}

impl SpotifySink {
    pub fn new() -> SpotifySink {
        // By setting the sync_channel bound to at least the output frame size of one resampling
        // step (1120 for a chunk size of 1024 and our frequency settings) the number of
        // synchronizations needed between SpotifySink::write and SpotifySink::read can be reduced.
        let (sender, receiver) = sync_channel::<[f32; 2]>(1120);

        SpotifySink {
            event_channel: ArcSwapOption::new(None),
            sender: Arc::new(sender),
            receiver: Arc::new(Mutex::new(receiver)),
            ended: false,
        }
    }
}

impl audio_backend::Sink for SpotifySink {
    fn start(&mut self) -> SinkResult<()> {
        Ok(())
    }

    fn stop(&mut self) -> SinkResult<()> {
        Ok(())
    }

    fn write(&mut self, packet: AudioPacket, converter: &mut Converter) -> SinkResult<()> {
        let samples = converter.f64_to_f32(packet.samples().unwrap());
        for chunk in samples.chunks_exact(2) {
            self.sender.send([chunk[0], chunk[1]]).unwrap()
        }

        Ok(())
    }
}

impl io::Read for SpotifySink {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let receiver = self.receiver.lock().unwrap();

        let mut bytes_written = 0;
        let sample_size = std::mem::size_of::<f32>() * 2;

        while bytes_written + (sample_size - 1) < buf.len() {
            match self
                .event_channel
                .load_full()
                .unwrap()
                .lock()
                .unwrap()
                .try_recv()
            {
                Ok(PlayerEvent::EndOfTrack { .. }) | Ok(PlayerEvent::Stopped { .. }) => {
                    self.ended = true;
                    return Ok(0);
                }
                Err(_) => {}
                _ => {}
            }

            if let Ok(sample) = receiver.try_recv() {
                if self.ended {
                    self.ended = false;
                    return Ok(0);
                }

                LittleEndian::write_f32_into(
                    &sample,
                    &mut buf[bytes_written..(bytes_written + sample_size)],
                );
                bytes_written += sample_size;
            }

            if self.ended {
                return Ok(0);
            }
        }

        Ok(bytes_written)
    }
}

impl io::Seek for SpotifySink {
    fn seek(&mut self, _pos: io::SeekFrom) -> io::Result<u64> {
        unreachable!()
    }
}

// Create a songbird MediaSource sink for Spotify
impl MediaSource for SpotifySink {
    fn is_seekable(&self) -> bool {
        false
    }

    fn byte_len(&self) -> Option<u64> {
        None
    }
}

impl Default for SpotifySink {
    fn default() -> Self {
        Self::new()
    }
}

impl Clone for SpotifySink {
    fn clone(&self) -> SpotifySink {
        SpotifySink {
            event_channel: ArcSwapOption::new(self.event_channel.load().clone()),
            receiver: self.receiver.clone(),
            sender: self.sender.clone(),
            ended: self.ended,
        }
    }
}
