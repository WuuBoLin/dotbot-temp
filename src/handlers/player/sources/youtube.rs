use crate::handlers::player::queue::queue;
use crate::handlers::player::{Mode, Source, Track, PLAYLIST_FETCHER_HANDLES};
use crate::result::errors::DotError;
use crate::result::DotResult;
use crate::time;
use once_cell::sync::Lazy;
use regex::Regex;
use reqwest::cookie::Jar;
use reqwest::header::{HeaderMap, ACCEPT_LANGUAGE, ORIGIN, REFERER, USER_AGENT};
use reqwest::{Client, ClientBuilder};
use serde_json::{json, Value};
use serenity::model::id::{GuildId, UserId};
use songbird::constants::SAMPLE_RATE_RAW;
use std::sync::Arc;
use std::{env, process::Stdio, time::Duration};
use tokio::{process::Command as TokioCommand, task};

use crate::client::BOT_USERID;
use crate::database::redis::REDIS;
use crate::messages::queue::EMBED_PAGE_SIZE;

pub static HTTP: Lazy<Client> = Lazy::new(|| {
    ClientBuilder::new()
        .connect_timeout(Duration::from_secs(7))
        .cookie_provider(COOKIE_JAR.clone())
        .default_headers(headers(false, false))
        .use_rustls_tls()
        .build()
        .unwrap()
});

pub static YOUTUBE_QUERY_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"^((?:https?:)?//)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(/(?:[\w\-]+\?v=|embed/|v/)?)([\w\-]+)(\S+)?$").unwrap()
});
pub static YOUTUBE_WATCH_PLAYLIST_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"^(?:https?://)?(?:www\.)?youtu(?:be\.com|\.be)/(?:watch\?[^&]*&)?list=(?P<id>[\w-]+)",
    )
    .unwrap()
});

pub static CONTEXT: Lazy<Value> = Lazy::new(|| {
    json!({
        "client": {
            "browserName": "Chrome",
            "browserVersion": "104.0.5112.81",
            "clientFormFactor": "UNKNOWN_FORM_FACTOR",
            "clientName": "WEB",
            "clientScreen": "WATCH",
            "clientVersion": "2.20220809.02.00",
            "gl": *REGION,
            "hl": "zh-TW",
            "timeZone": "Asia/Taipei",
            "utcOffsetMinutes": 480,
            "userAgent": USER_AGENTS[0],
            "visitorData": "CgtIZDE3aXJqLXFwNCiZpPaHBg%3D%3D"
        }
    })
});

pub static MUSIC_CONTEXT: Lazy<Value> = Lazy::new(|| {
    json!({
        "client": {
            "browserName": "Chrome",
            "browserVersion": "104.0.5112.81",
            "clientFormFactor": "UNKNOWN_FORM_FACTOR",
            "clientName": "WEB_REMIX",
            "clientVersion": "1.20220808.01.00",
            "originalUrl": "https://music.youtube.com/watch?v=QPJ15esipsU",
            "gl": *REGION,
            "hl": "zh-TW",
            "timeZone": "Asia/Taipei",
            "utcOffsetMinutes": 480,
            "userAgent": USER_AGENTS[0],
            "visitorData": "CgtIZDE3aXJqLXFwNCiZpPaHBg%3D%3D"
        }
    })
});

pub const USER_AGENTS: &[&str] = &[
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36 Edg/101.0.1210.53",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36 Edg/99.0.1150.30",
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 YaBrowser/19.10.3.281 Yowser/2.5 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36",
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36",
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36"
];

pub static REGION: Lazy<String> =
    Lazy::new(|| env::var("REGION").unwrap_or_else(|_| "TW".to_string()));

pub static COOKIE_JAR: Lazy<Arc<Jar>> = Lazy::new(|| Arc::new(cookies(DEFAULT_COOKIE.to_string())));

pub const DEFAULT_COOKIE: &str = "GPS=1; PREF=tz=Asia.Taipei&f6=40000000; CONSENT=YES;";

pub const YOUTUBE_ENDPOINT: &str = "https://www.youtube.com";

const NEWLINE_BYTE: u8 = 0xA;

fn cookies(default: String) -> Jar {
    let url = YOUTUBE_ENDPOINT.parse().unwrap();
    let jar = Jar::default();

    for str in default.split(';').map(|x| x.trim()) {
        jar.add_cookie_str(format!("{str}; Priority=HIGH; Path=/; Domain=.youtube.com; HttpOnly; Secure; Expires=Fri, 01 Jan 2038 00:00:00 GMT;").as_str(), &url);
    }

    jar
}

fn headers(_auth: bool, music: bool) -> HeaderMap {
    let mut headers = HeaderMap::new();

    if music {
        headers.insert(ORIGIN, "https://music.youtube.com".parse().unwrap());
        headers.insert("X-Origin", "https://music.youtube.com".parse().unwrap());
        headers.insert("X-Youtube-Client-Name", "67".parse().unwrap());
        headers.insert(
            "X-Youtube-Client-Version",
            "1.20220808.01.00".parse().unwrap(),
        );

        headers.insert(REFERER, "https://music.youtube.com/".parse().unwrap());
    } else {
        headers.insert(ORIGIN, YOUTUBE_ENDPOINT.parse().unwrap());
        headers.insert("X-Origin", YOUTUBE_ENDPOINT.parse().unwrap());
        headers.insert("X-Youtube-Client-Name", "1".parse().unwrap());
        headers.insert("X-Youtube-Time-Zone", "Asia/Taipei".parse().unwrap());
        headers.insert("X-Youtube-Device", "cbr=Edge+Chromium&cbrver=105.0.1343.33&ceng=WebKit&cengver=537.36&cos=Windows&cosver=10.0&cplatform=DESKTOP&cyear=2011".parse().unwrap());
        headers.insert(
            "X-Youtube-Client-Version",
            "2.20210721.00.00".parse().unwrap(),
        );

        headers.insert(
            REFERER,
            "https://www.youtube.com/watch?v=jfKfPfyJRdk"
                .parse()
                .unwrap(),
        );
    }

    headers.insert(ACCEPT_LANGUAGE, "zh-TW,zh;q=0.9,en;q=0.8".parse().unwrap());
    headers.insert(
        USER_AGENT,
        USER_AGENTS[(rand::random::<f32>() * USER_AGENTS.len() as f32).floor() as usize]
            .parse()
            .unwrap(),
    );

    headers.insert("sec-fetch-mode", "navigate".parse().unwrap());

    headers
}

pub struct YouTube {}

impl YouTube {
    pub async fn extract(
        query: &str,
        requested_by: (UserId, GuildId),
        mode: Option<Mode>,
    ) -> DotResult<Vec<Track>> {
        let captures = YOUTUBE_QUERY_REGEX
            .captures(query)
            .ok_or(DotError::TrackFail)?;

        let Some(id) = captures.get(5) else {
            return Err(DotError::TrackFail);
        };

        if id.as_str() == "playlist" {
            if captures.get(6).is_none() {
                return Err(DotError::TrackFail)?;
            };
            let playlist_id = captures.get(6).unwrap().as_str().replace("?list=", "");
            return Self::playlist(&playlist_id, requested_by, mode).await;
        }

        if captures.get(6).is_some() {
            return match mode {
                Some(Mode::All) => {
                    let Some(playlist_id) = YOUTUBE_WATCH_PLAYLIST_REGEX
                        .captures(query)
                        .ok_or(DotError::TrackFail)?
                        .get(1)
                    else {
                        return Err(DotError::TrackFail);
                    };
                    Self::playlist(playlist_id.as_str(), requested_by, mode).await
                }
                _ => Self::search(captures.get(5).unwrap().as_str(), 1, requested_by.0).await,
            };
        }

        Self::search(captures.get(5).unwrap().as_str(), 1, requested_by.0).await
    }

    pub async fn search(query: &str, limit: u32, requested_by: UserId) -> DotResult<Vec<Track>> {
        let result = HTTP
            .post("https://www.youtube.com/youtubei/v1/search?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8&prettyPrint=false")
            .headers(headers(false, false))
            .json(&json!({
            "context": *CONTEXT,
            "query": query,
            "params": "EgIQAQ%3D%3D"
        }))
            .send()
            .await
            .map_err(|_| DotError::TrackFail)?
            .json::<Value>()
            .await
            .map_err(|_| DotError::TrackFail)?;

        let infos = result["contents"]["twoColumnSearchResultsRenderer"]["primaryContents"]
            ["sectionListRenderer"]["contents"][0]["itemSectionRenderer"]["contents"]
            .as_array();

        if infos.is_none() {
            return Err(DotError::TrackFail);
        }

        let mut tracks = Vec::with_capacity(infos.unwrap().len());

        for video in infos.unwrap() {
            if tracks.len() >= limit as usize {
                break;
            }

            let id = video["videoRenderer"]["videoId"].as_str();

            if id.is_none() {
                continue;
            }

            let id = id.unwrap();

            let channel = video["videoRenderer"]["ownerText"]["runs"].get(0);
            let Some(channel) = channel
                .and_then(|c| c["text"].as_str())
                .map(|x| x.to_string())
            else {
                return Err(DotError::TrackFail);
            };

            let duration_raw = video["videoRenderer"]["lengthText"]["simpleText"]
                .as_str()
                .unwrap_or("00:00")
                .to_string();

            let duration_ms = time::duration_hms_to_ms(duration_raw)?;

            tracks.push(Track {
                id: id.to_string(),
                title: video["videoRenderer"]["title"]["runs"]
                    .get(0)
                    .and_then(|run| run["text"].as_str())
                    .unwrap_or("Untitled")
                    .to_string(),
                source: Source::YouTube,
                artists: vec![channel],
                requested_by,
                is_live: duration_ms == 0,
                duration_ms,
                thumbnail_url: Self::format_thumbnail(
                    &video["videoRenderer"]["thumbnail"]["thumbnails"],
                ),
                start_time: None,
                start_from: 0,
            })
        }

        Ok(tracks)
    }

    pub async fn playlist(
        id: &str,
        requested_by: (UserId, GuildId),
        mode: Option<Mode>,
    ) -> DotResult<Vec<Track>> {
        let result = HTTP
            .post("https://www.youtube.com/youtubei/v1/browse?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8&prettyPrint=false")
            .headers(headers(true, false))
            .json(&json!({
            "context": *CONTEXT,
            "browseId": format!("VL{id}"),
        }))
            .send()
            .await
            .map_err(|_| DotError::TrackFail)?
            .json::<Value>()
            .await
            .map_err(|_| DotError::TrackFail)?;

        if result["alerts"][0]["alertRenderer"]["type"].as_str() == Some("ERROR") {
            return Err(DotError::TrackFail);
        }

        let data = &result["header"]["playlistHeaderRenderer"];

        if matches!(*data, serde_json::Value::Null) {
            return Err(DotError::TrackFail);
        }

        let mut raws = result["contents"]["twoColumnBrowseResultsRenderer"]["tabs"][0]
            ["tabRenderer"]["content"]["sectionListRenderer"]["contents"][0]["itemSectionRenderer"]
            ["contents"][0]["playlistVideoListRenderer"]["contents"]
            .as_array()
            .unwrap()
            .to_owned();

        let mut tracks = Vec::with_capacity(100);
        let mut token = None;

        while tracks.len() < 100 {
            if token.is_none() && raws.is_empty() {
                break;
            }

            if token.is_some() {
                raws = Self::playlist_continue(token.unwrap()).await?;
                token = None;
            }

            for data in &raws {
                if let Some(next_token) = data["continuationItemRenderer"]["continuationEndpoint"]
                    ["continuationCommand"]["token"]
                    .as_str()
                {
                    token = Some(next_token.to_string());
                    break;
                }

                let track =
                    Self::parse_playlist_video(&data["playlistVideoRenderer"], requested_by.0);

                if track.is_none() {
                    continue;
                }

                tracks.push(track.unwrap());
            }

            raws.clear();
        }

        if token.is_some() {
            let mut other_tracks = Vec::new();
            let token_clone = token.clone();
            let playlist_fetcher_handle = task::spawn(async move {
                let mut raws = Vec::new();
                let mut token = token_clone;
                loop {
                    if token.is_none() && raws.is_empty() {
                        break;
                    }

                    if token.is_some() {
                        raws = Self::playlist_continue(token.unwrap())
                            .await
                            .ok()
                            .unwrap_or(vec![]);
                        token = None;
                    }

                    for data in &raws {
                        if let Some(next_token) = data["continuationItemRenderer"]
                            ["continuationEndpoint"]["continuationCommand"]["token"]
                            .as_str()
                        {
                            token = Some(next_token.to_string());
                            break;
                        }

                        let track = Self::parse_playlist_video(
                            &data["playlistVideoRenderer"],
                            requested_by.0,
                        );

                        if track.is_none() {
                            continue;
                        }

                        other_tracks.push(track.unwrap());
                    }

                    raws.clear();
                }

                let mode = mode.unwrap_or(Mode::End);

                queue(other_tracks, requested_by.1, mode).await.ok();
            });

            if let Some(handles) = PLAYLIST_FETCHER_HANDLES.get(&requested_by.1) {
                handles
                    .lock()
                    .await
                    .push(playlist_fetcher_handle.abort_handle());
            } else {
                PLAYLIST_FETCHER_HANDLES.insert(
                    requested_by.1,
                    Arc::new(tokio::sync::Mutex::new(vec![
                        playlist_fetcher_handle.abort_handle()
                    ])),
                );
            }
        }

        if tracks.is_empty() {
            return Err(DotError::TrackFail);
        }

        Ok(tracks)
    }

    async fn playlist_continue(token: String) -> DotResult<Vec<Value>> {
        let result = HTTP
            .post("https://www.youtube.com/youtubei/v1/browse?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8&prettyPrint=false")
            .headers(headers(true, false))
            .json(&json!({
            "context": *CONTEXT,
            "continuation": token,
        }))
            .send()
            .await
            .map_err(|_| DotError::TrackFail)?
            .json::<Value>()
            .await
            .map_err(|_| DotError::TrackFail)?;

        Ok(
            result["onResponseReceivedActions"][0]["appendContinuationItemsAction"]
                ["continuationItems"]
                .as_array()
                .unwrap_or(&vec![])
                .to_owned(),
        )
    }

    fn parse_playlist_video(data: &Value, requested_by: UserId) -> Option<Track> {
        let Some(id) = data["videoId"].as_str() else {
            return None;
        };

        if !data["isPlayable"].as_bool().unwrap_or(false) {
            return None;
        }

        let duration_in_sec: u32 = data["lengthSeconds"]
            .as_str()
            .unwrap_or("0")
            .parse()
            .unwrap();

        let channel = &data["shortBylineText"]["runs"].as_array().unwrap()[0];

        Some(Track {
            id: id.to_string(),
            title: data["title"]["runs"][0]["text"]
                .as_str()
                .unwrap_or("未知的標題")
                .to_string(),
            duration_ms: duration_in_sec * 1000,
            artists: vec![channel["text"]
                .as_str()
                .map(|x| x.to_string())
                .unwrap_or("Unknown".to_string())],
            requested_by,
            is_live: duration_in_sec == 0,
            source: Source::YouTube,
            thumbnail_url: Self::format_thumbnail(&data["thumbnail"]["thumbnails"]),
            start_time: None,
            start_from: 0,
        })
    }

    fn format_thumbnail(thumbnails: &Value) -> Option<String> {
        thumbnails.as_array()?;

        thumbnails
            .as_array()
            .unwrap()
            .iter()
            .max_by(|x, v| {
                x["width"]
                    .as_u64()
                    .unwrap_or(0)
                    .cmp(&v["width"].as_u64().unwrap_or(0))
            })
            .and_then(|t| t["url"].as_str())
            .and_then(|t| t.split('?').next())
            .map(|t| t.to_owned())
    }

    pub async fn recommend(guild_id: GuildId, now_track: Track, queue_len: u64) -> DotResult<()> {
        let track_id = now_track.id;
        let result = HTTP
            .post("https://music.youtube.com/youtubei/v1/next?key=AIzaSyC9XL3ZjWddXya6X74dJoCTL-WEYFDNX30&prettyPrint=false")
            .headers(headers(false, true))
            .json(&json!({
		"context": *MUSIC_CONTEXT,
		"enablePersistentPlaylistPanel": true,
		"isAudioOnly": true,
		"videoId": track_id,
		"params": "8gEAmgMDCNgE",
		"playerParams": "igMDCNgEoAME",
		"playlistId": format!("RDAMVM{track_id}"),
            }))
	    .send()
	    .await
	    .map_err(|_| DotError::ErrorRecommend)?
	    .json::<Value>()
	    .await
	    .map_err(|_| DotError::ErrorRecommend)?;

        let mut videos = result["contents"]["singleColumnMusicWatchNextResultsRenderer"]
            ["tabbedRenderer"]["watchNextTabbedResultsRenderer"]["tabs"]
            .as_array()
            .and_then(|tabs| tabs.get(0))
            .and_then(|tab| {
                tab["tabRenderer"]["content"]["musicQueueRenderer"]["content"]
                    ["playlistPanelRenderer"]["contents"]
                    .as_array()
            })
            .unwrap_or(&vec![])
            .to_owned();

        if let Some(position) = videos
            .iter()
            .position(|v| v["playlistPanelVideoRenderer"]["videoId"].as_str() == Some(&track_id))
        {
            videos.remove(position);
        }

        if videos.len() < 2 {
            return Err(DotError::ErrorRecommend);
        }

        let needed = (EMBED_PAGE_SIZE - queue_len) as usize;
        let mut recommended_tracks = Vec::with_capacity(needed);

        for video in &videos[1..] {
            if recommended_tracks.len() >= needed {
                break;
            }

            let video = &video["playlistPanelVideoRenderer"];

            let (Some(id), Some(title)) = (
                video["videoId"].as_str(),
                video["title"]["runs"]
                    .as_array()
                    .and_then(|runs| runs.get(0))
                    .and_then(|run| run["text"].as_str()),
            ) else {
                continue;
            };

            let duration_raw = video["lengthText"]["runs"]
                .as_array()
                .and_then(|runs| runs.get(0))
                .and_then(|run| run["text"].as_str())
                .unwrap_or("00:00")
                .to_string();

            let duration: Vec<_> = duration_raw.rsplit(':').collect();
            let mut duration_in_sec = 0;
            let mut base = 1;
            for time in duration {
                duration_in_sec += time.parse().unwrap_or(0) * base;
                base *= 60;
            }

            let channel = video["shortBylineText"]["runs"]
                .as_array()
                .and_then(|channel| channel.get(0))
                .and_then(|channel| channel["text"].as_str().map(|x| x.to_string()))
                .unwrap_or("Unknown".to_string());

            let track = Track {
                id: id.to_string(),
                title: title.to_string(),
                source: Source::YouTube,
                artists: vec![channel],
                requested_by: *BOT_USERID.get().unwrap(),
                is_live: false,
                duration_ms: duration_in_sec * 1000,
                thumbnail_url: Self::format_thumbnail(&video["thumbnail"]["thumbnails"]),
                start_time: None,
                start_from: 0,
            };

            recommended_tracks.push(track);
        }

        if recommended_tracks.is_empty() {
            return Err(DotError::ErrorRecommend);
        }

        let mut q = REDIS.queue_ctl().await;
        q.append(&guild_id.to_string(), recommended_tracks.as_slice())
            .await
            .unwrap();

        Ok(())
    }
}

#[derive(Default)]
pub struct YoutubeMetadata {
    pub track: Option<String>,
    pub artist: Option<String>,
    pub date: Option<String>,
    pub channels: Option<u8>,
    pub channel: Option<String>,
    pub start_time: Option<Duration>,
    pub duration: Option<Duration>,
    pub sample_rate: Option<u32>,
    pub source_url: Option<String>,
    pub title: Option<String>,
    pub thumbnail: Option<String>,
}

impl YoutubeMetadata {
    pub async fn get(uri: &str) -> DotResult<YoutubeMetadata> {
        let ytdl_args = [
            "-j", // print JSON information for video for metadata
            "-R",
            "infinite",        // infinite number of download retries
            "--no-playlist",   // only download the video if URL also has playlist info
            "--ignore-config", // disable all configuration files for a yt-dlp run
            uri,
            "-o",
            "-", // stream data to stdout
        ];

        let youtube_dl_output = TokioCommand::new("yt-dlp")
            .args(ytdl_args)
            .stdin(Stdio::null())
            .output()
            .await
            .map_err(|_| DotError::TrackFail)?;

        let o_vec = youtube_dl_output.stderr;

        // read until newline byte
        let end = (o_vec)
            .iter()
            .position(|el| *el == NEWLINE_BYTE)
            .unwrap_or(o_vec.len());

        let value = serde_json::from_slice::<Value>(&o_vec[..end])
            .map_err(|_| DotError::Other("Cannot Parse Youtube Data!"))?;
        let obj = value.as_object();

        let track = obj
            .and_then(|m| m.get("track"))
            .and_then(Value::as_str)
            .map(str::to_string);

        let true_artist = obj
            .and_then(|m| m.get("artist"))
            .and_then(Value::as_str)
            .map(str::to_string);

        let artist = true_artist.or_else(|| {
            obj.and_then(|m| m.get("uploader"))
                .and_then(Value::as_str)
                .map(str::to_string)
        });

        let r_date = obj
            .and_then(|m| m.get("release_date"))
            .and_then(Value::as_str)
            .map(str::to_string);

        let date = r_date.or_else(|| {
            obj.and_then(|m| m.get("upload_date"))
                .and_then(Value::as_str)
                .map(str::to_string)
        });

        let channel = obj
            .and_then(|m| m.get("channel"))
            .and_then(Value::as_str)
            .map(str::to_string);

        let duration = obj
            .and_then(|m| m.get("duration"))
            .and_then(Value::as_f64)
            .map(Duration::from_secs_f64);

        let source_url = obj
            .and_then(|m| m.get("webpage_url"))
            .and_then(Value::as_str)
            .map(str::to_string);

        let title = obj
            .and_then(|m| m.get("title"))
            .and_then(Value::as_str)
            .map(str::to_string);

        let thumbnail = obj
            .and_then(|m| m.get("thumbnail"))
            .and_then(Value::as_str)
            .map(str::to_string);

        Ok(Self {
            track,
            artist,
            date,

            channels: Some(2),
            channel,
            duration,
            sample_rate: Some(SAMPLE_RATE_RAW as u32),
            source_url,
            title,
            thumbnail,

            ..Default::default()
        })
    }
}
