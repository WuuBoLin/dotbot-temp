use std::time::Duration;

use poise::serenity_prelude::{GuildId, UserId};

use crate::{
    handlers::player::{Source, Track},
    result::DotResult,
};

use super::youtube::YoutubeMetadata;

pub struct Ambiguous;

impl Ambiguous {
    pub async fn extract(query: &str, requested_by: (UserId, GuildId)) -> DotResult<Vec<Track>> {
        let metadata = YoutubeMetadata::get(query).await?;

        Ok(vec![Track {
            id: query.to_string(),
            title: metadata.title.unwrap_or_else(|| "Unknown".to_string()),
            source: Source::Ambiguous,
            artists: vec![metadata.artist.unwrap_or_else(|| "Unknown".to_string())],
            requested_by: requested_by.0,
            is_live: false,
            duration_ms: metadata
                .duration
                .unwrap_or(Duration::from_millis(0))
                .as_millis() as u32,
            thumbnail_url: metadata.thumbnail,
            start_time: None,
            start_from: 0,
        }])
    }
}
