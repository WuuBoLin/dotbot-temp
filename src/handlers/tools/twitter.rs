use crate::database::redis::REDIS;
use crate::result::DotResult;
use poise::serenity_prelude as serenity;
use regex::Regex;
use serenity::all::Message;

pub async fn fix_handler(ctx: &serenity::Context, msg: &Message) -> DotResult<()> {
    let regex = Regex::new(r"https?://(twitter\.com|x\.com)([^\n?]*)(\?[^\n#]*)?").unwrap();

    if let Some(path) = regex.captures(&msg.content).and_then(|c| c.get(2)) {
        if path.as_str() == "/" {
            return Ok(());
        }
    } else {
        return Ok(());
    }

    let Some(guild_id) = msg.guild_id.and_then(|g| Some(g.to_string())) else {
	    return Ok(());
    };

    match REDIS.json_get::<bool>(&guild_id, "$.twitter_fix").await {
        Some(fix) => {
            if !fix {
                return Ok(());
            }
        }
        None => {
            REDIS
                .json_set::<bool>(&guild_id, "$.twitter_fix", &true)
                .await
                .unwrap();
        }
    };

    let modified = regex.replace_all(&msg.content, |caps: &regex::Captures| {
        let domain = &caps[1];
        let rest = if caps.len() > 2 { &caps[2] } else { "" };

        match domain {
            "twitter.com" => format!("https://fxtwitter.com{}", rest),
            "x.com" => format!("https://fixupx.com{}", rest),
            _ => String::from(""),
        }
    });

    msg.reply(ctx, modified).await?;

    Ok(())
}
