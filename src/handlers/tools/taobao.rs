use crate::database::redis::REDIS;
use crate::result::errors::DotError;
use crate::result::DotResult;
use poise::serenity_prelude as serenity;
use regex::Regex;
use serenity::all::{CreateButton, CreateEmbed, Message};
use serenity::builder::{CreateActionRow, CreateAllowedMentions, CreateEmbedFooter, CreateMessage};
use serenity::model::Colour;
use url::Url;

pub async fn fix_handler(ctx: &serenity::Context, msg: &Message) -> DotResult<()> {
    let regex =
        Regex::new(r"(https?://)?(m\.tb\.cn|a\.m\.taobao\.com|item\.taobao\.com)/\S*").unwrap();

    let orig_urls: Vec<_> = regex
        .find_iter(&msg.content)
        .map(|m| m.as_str().to_string())
        .collect();

    let Some(guild_id) = msg.guild_id.and_then(|g| Some(g.to_string())) else {
	    return Ok(());
    };

    match REDIS.json_get::<bool>(&guild_id, "$.taobao_fix").await {
        Some(fix) => {
            if !fix {
                return Ok(());
            }
        }
        None => {
            REDIS
                .json_set::<bool>(&guild_id, "$.taobao_fix", &true)
                .await
                .unwrap();
        }
    };

    for orig_url in orig_urls {
        let (ctx, msg) = (ctx.clone(), msg.clone());
        tokio::task::spawn(async move {
            let Some(og) = (match Url::parse(&orig_url) {
                Ok(url) => match url.host_str() {
                    Some("a.m.taobao.com") => a_m_taobao_com(url).await,
                    Some("m.tb.cn") => m_tb_cn(url).await,
                    Some("item.taobao.com") => item_taobao_com(url).await,
                    None => return Err(DotError::Other("taobao fix failed")),
                    _ => return Err(DotError::Other("taobao fix failed")),
                },
                Err(_) => return Err(DotError::Other("taobao fix failed")),
            }) else {
                return Ok(());
            };

            let (url, image, description) = match (og.url, og.image, og.description) {
                (Some(url), Some(image), Some(description)) => (url, image, description),
                _ => return Ok(()),
            };

            let mut buttons = if orig_url.len() > 500 {
                vec![CreateButton::new_link(url).label("淘寶全球")]
            } else {
                vec![
                    CreateButton::new_link(&orig_url).label("原連結"),
                    CreateButton::new_link(url).label("淘寶全球"),
                ]
            };
            if let Some(video) = og.video_src {
                buttons.push(CreateButton::new_link(video).label("影片"));
            }

            msg.channel_id
                .send_message(
                    &ctx.http,
                    CreateMessage::new()
                        .embed(
                            CreateEmbed::default()
                                .title(og.title)
                                .description(description)
                                .color(Colour::from_rgb(0, 168, 252))
                                .image(image)
                                .url(&orig_url)
                                .footer(
                                    CreateEmbedFooter::new("淘寶")
                                        .icon_url("https://i.imgur.com/Tt5t9Dk.png"),
                                ),
                        )
                        .components(vec![CreateActionRow::Buttons(buttons)])
                        .reference_message(&msg)
                        .allowed_mentions(
                            CreateAllowedMentions::new()
                                .empty_roles()
                                .empty_users()
                                .replied_user(false),
                        ),
                )
                .await
                .ok();

            Ok(())
        });
    }

    Ok(())
}

pub async fn a_m_taobao_com(url: impl ToString) -> Option<TaobaoOG> {
    let regex = Regex::new(r"https?://a\.m\.taobao\.com/([^/]+)\.(?:html?|htm)").unwrap();

    let url = url.to_string();
    let mut item_id = regex
        .captures(&url)
        .and_then(|c| c.get(1))?
        .as_str()
        .to_string();
    item_id.remove(0);

    get_og(item_id).await
}

pub async fn m_tb_cn(url: impl ToString) -> Option<TaobaoOG> {
    let html = reqwest::Client::new()
        .get(url.to_string())
        .send()
        .await
        .map_err(|_| DotError::Other("taobao fix failed"))
        .ok()?
        .text()
        .await
        .map_err(|_| DotError::Other("taobao fix failed"))
        .ok()?;

    item_taobao_com(html).await
}

pub async fn item_taobao_com(url: impl ToString) -> Option<TaobaoOG> {
    let regex = Regex::new(
        r"https?://item\.taobao\.com/[^?]*\??.*\b(?:id=([^&]+)|\?.*id=([^&]+)|/([^/?]+))(?:&.*)?",
    )
    .unwrap();

    let url = url.to_string();
    let item_id = regex.captures(&url).and_then(|c| c.get(1))?;

    get_og(item_id.as_str()).await
}

pub async fn get_og(item_id: impl ToString) -> Option<TaobaoOG> {
    let taobao_url = format!("https://world.taobao.com/item/{}.html", item_id.to_string());
    let og_regex = |prop: &str| {
        Regex::new(format!(r#"<meta\s+property="og:{prop}"\s+content="([^"]+)"\s*\/>"#).as_str())
            .unwrap()
    };

    let html = reqwest::Client::new()
        .get(taobao_url)
        .send()
        .await
        .map_err(|_| DotError::Other("taobao fix failed"))
        .ok()?
        .text()
        .await
        .map_err(|_| DotError::Other("taobao fix failed"))
        .ok()?;

    let Some(title) = Regex::new("<title>(.*?)</title>")
        .unwrap()
        .captures(&html)
        .and_then(|c| c.get(1))
    else {
        return None;
    };

    Some(TaobaoOG {
        title: title.as_str().to_string(),
        url: og_regex("url")
            .captures(html.as_str())
            .and_then(|c| c.get(1))
            .map(|c| c.as_str().to_string()),
        image: og_regex("image")
            .captures(html.as_str())
            .and_then(|c| c.get(1))
            .map(|c| c.as_str().to_string()),
        video_src: og_regex("videosrc")
            .captures(html.as_str())
            .and_then(|c| c.get(1))
            .map(|c| c.as_str().to_string()),
        description: og_regex("description")
            .captures(html.as_str())
            .and_then(|c| c.get(1))
            .map(|c| c.as_str().to_string()),
    })
}

#[derive(Debug, Default)]
pub struct TaobaoOG {
    title: String,
    url: Option<String>,
    image: Option<String>,
    video_src: Option<String>,
    description: Option<String>,
}
