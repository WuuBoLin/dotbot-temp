use tracing::error;
use tracing::level_filters::LevelFilter;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use tracing_subscriber::{fmt, reload};

use dotbot::client::Client;
use dotbot::result::DotResult;
use dotbot::rpc;
use dotbot::rpc::tracing::TRACING_HANDLE;

#[tokio::main]
async fn main() -> DotResult<()> {
    let (filter, reload_handle) = reload::Layer::new(LevelFilter::INFO);
    TRACING_HANDLE.set(reload_handle).unwrap();
    tracing_subscriber::registry()
        .with(filter)
        .with(fmt::Layer::default())
        .init();

    dotenv::dotenv().ok();

    rpc::activate().await;

    let dotbot = Client::default().await?;
    if let Err(why) = dotbot.start().await {
        error!("Fatal! DotBot crashed because: {:?}", why);
    };

    Ok(())
}
