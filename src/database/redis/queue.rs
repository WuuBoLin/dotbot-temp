use crate::handlers::player::Track;
use deadpool_redis::Connection;
use redis::{JsonAsyncCommands, RedisResult, Value};
use serde_json::{from_str, json};

pub struct Queue {
    pub connection: Connection,
}

impl Queue {
    pub async fn clear(&mut self, key: &str) -> RedisResult<()> {
        self.connection
            .json_clear::<&str, &str, ()>(key, "$.queue")
            .await
    }

    pub async fn length(&mut self, key: &str) -> u64 {
        self.connection
            .json_arr_len::<&str, &str, Vec<u64>>(key, "$.queue")
            .await
            .ok()
            .and_then(|x| x.into_iter().next().take())
            .unwrap_or(0)
    }

    pub async fn pop(&mut self, key: &str) -> RedisResult<()> {
        self.connection
            .json_arr_pop::<&str, &str, ()>(key, "$.queue", 0)
            .await
    }

    pub async fn remove(&mut self, key: &str, index: u64) -> RedisResult<()> {
        self.connection
            .json_arr_pop::<&str, &str, ()>(key, "$.queue", index as i64)
            .await
    }

    pub async fn first_track(&mut self, key: &str) -> Option<Track> {
        self.get_track(key, 0).await
    }

    pub async fn get_track(&mut self, key: &str, index: u64) -> Option<Track> {
        self.get_tracks(key, index, index + 1)
            .await
            .and_then(|x| x.into_iter().next().take())
    }

    pub async fn get_tracks(
        &mut self,
        key: &str,
        from_index: u64,
        count: u64,
    ) -> Option<Vec<Track>> {
        self.connection
            .json_get::<&str, &str, String>(key, &*format!("$.queue[{}:{}]", from_index, count))
            .await
            .ok()
            .and_then(|x| from_str::<Vec<Track>>(&x).ok())
    }

    pub async fn trim_from(&mut self, key: &str, index: u64) -> RedisResult<()> {
        let length = self.length(key).await;
        self.connection
            .json_arr_trim::<&str, &str, ()>(key, "$.queue", index as i64, length as i64)
            .await
    }

    pub async fn append(&mut self, key: &str, tracks: &[Track]) -> RedisResult<Value> {
        redis::cmd("JSON.ARRAPPEND")
            .arg(key)
            .arg("$.queue")
            .arg(Self::tracks_to_strings_of_json(tracks))
            .query_async::<_, Value>(&mut self.connection)
            .await
    }

    pub async fn insert(&mut self, key: &str, tracks: &[Track]) -> RedisResult<Value> {
        redis::cmd("JSON.ARRINSERT")
            .arg(key)
            .arg("$.queue")
            .arg(0)
            .arg(Self::tracks_to_strings_of_json(tracks))
            .query_async::<_, Value>(&mut self.connection)
            .await
    }

    pub async fn replace_track(&mut self, key: &str, index: u64, track: &Track) -> RedisResult<()> {
        let path = format!("$.queue[{index}]");

        self.connection
            .json_set::<&str, &str, Track, ()>(key, &path, track)
            .await
    }

    fn tracks_to_strings_of_json(tracks: &[Track]) -> Vec<String> {
        tracks
            .iter()
            .map(|t| json!(t).to_string())
            .collect::<Vec<String>>()
    }
}
