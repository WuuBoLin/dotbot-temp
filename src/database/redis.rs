pub mod queue;

use std::env;

use crate::database::redis::queue::Queue;
use crate::handlers::player::Track;
use crate::result::errors::DotError;
use crate::result::DotResult;
use deadpool_redis::{Config, Connection, Pool, Runtime::Tokio1};
use once_cell::sync::Lazy;
use rand::seq::SliceRandom;
use redis::{AsyncCommands, JsonAsyncCommands, RedisResult, Value};
use serde::{de::DeserializeOwned, Serialize};
use serde_json::{from_str, json};

pub static REDIS: Lazy<Redis> = Lazy::new(Redis::default);

pub struct Redis {
    pub pool: Pool,
}

impl Default for Redis {
    fn default() -> Self {
        let builder = Config::from_url(env::var("REDIS_URL").unwrap())
            .builder()
            .unwrap()
            .max_size(8)
            .runtime(Tokio1);

        Self {
            pool: builder.build().unwrap(),
        }
    }
}

impl Redis {
    pub async fn get_conn(&self) -> Connection {
        self.pool.get().await.unwrap()
    }

    pub async fn json_get<T>(&self, key: &str, path: &str) -> Option<T>
    where
        T: DeserializeOwned,
    {
        self.get_conn()
            .await
            .json_get::<&str, &str, String>(key, path)
            .await
            .ok()
            .and_then(|x| from_str::<Vec<T>>(&x).ok())
            .and_then(|x| x.into_iter().next().take())
    }

    pub async fn json_set<T>(&self, key: &str, path: &str, value: &T) -> RedisResult<Value>
    where
        T: Serialize + Sync + Send,
    {
        let mut conn = self.get_conn().await;
        if conn
            .json_get::<&str, &str, String>(key, ".")
            .await
            .ok()
            .is_none()
        {
            conn.json_set::<&str, &str, serde_json::Value, Value>(key, ".", &json!({}))
                .await?;
        }
        conn.json_set::<&str, &str, T, Value>(key, path, value)
            .await
    }

    pub async fn get(&self, key: &str) -> Option<String> {
        self.get_conn().await.get::<&str, String>(key).await.ok()
    }

    pub async fn set(&self, key: &str, value: &str) -> RedisResult<()> {
        self.get_conn()
            .await
            .set::<&str, &str, ()>(key, value)
            .await
    }

    pub async fn shuffle(&self, key: &str) -> DotResult<()> {
        let Some(mut tracks) = self.json_get::<Vec<Track>>(key, "$.queue").await else {
            return Err(DotError::QueueEmpty);
        };
        tracks.shuffle(&mut rand::thread_rng());
        self.json_set(key, "$.queue", &tracks).await.unwrap();
        Ok(())
    }

    pub async fn queue_ctl(&self) -> Queue {
        Queue {
            connection: self.get_conn().await,
        }
    }
}
