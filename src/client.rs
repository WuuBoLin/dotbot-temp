use once_cell::sync::OnceCell;
use poise::builtins::register_globally;
use serenity::all::UserId;
use std::env;

use serenity::prelude::GatewayIntents;
use songbird::SerenityInit;
use tracing::info;

use crate::handlers::event_handler;
use crate::{
    commands::get_commands,
    handlers::{
        error_handler,
        player::sources::spotify::{Spotify, SpotifyApi},
    },
    result::{errors::DotError, DotResult},
};

pub const BOT_AVATAR: &str = "https://cdn.oxssxo.com/dot.png";

pub static BOT_USERID: OnceCell<UserId> = OnceCell::new();

pub struct Data {}

pub type Context<'a> = poise::Context<'a, Data, DotError>;

pub struct Client {
    framework_builder: poise::framework::FrameworkBuilder<Data, DotError>,
}

impl Client {
    pub async fn default() -> DotResult<Client> {
        let token = env::var("DISCORD_TOKEN").expect("Fatality! DISCORD_TOKEN not set!");
        Client::new(token).await
    }

    pub async fn new(token: String) -> DotResult<Client> {
        let gateway_intents = GatewayIntents::non_privileged() | GatewayIntents::privileged();

        let framework_builder = poise::FrameworkBuilder::default()
            .token(token)
            .intents(gateway_intents)
            .client_settings(|client_builder| client_builder.register_songbird())
            .options(poise::FrameworkOptions {
                commands: get_commands(),
                event_handler: |event, framework, _| {
                    Box::pin(async move { event_handler(event, framework).await })
                },
                on_error: |error| Box::pin(error_handler(error)),
                prefix_options: poise::PrefixFrameworkOptions {
                    prefix: Some('.'.into()),
                    edit_tracker: Some(poise::EditTracker::for_timespan(
                        std::time::Duration::from_secs(3600),
                    )),
                    mention_as_prefix: true,
                    execute_untracked_edits: true,
                    ..Default::default()
                },
                ..Default::default()
            })
            .setup(|ctx, ready, framework| {
                Box::pin(async move {
                    register_globally(ctx, &framework.options().commands).await?;
                    info!("🔵 {} is connected!", ready.user.name);
                    BOT_USERID.set(ready.user.id).unwrap();
                    Ok(Data {})
                })
            });

        Ok(Client { framework_builder })
    }

    pub async fn start(self) -> Result<(), serenity::Error> {
        Spotify::login().await.unwrap();
        SpotifyApi::auth().await.unwrap();

        self.framework_builder.run_autosharded().await
    }
}
