pub(crate) mod music;
pub(crate) mod settings;

use crate::result::errors::DotError;

pub fn get_commands() -> Vec<poise::Command<crate::client::Data, DotError>> {
    vec![
        music::play::play(),
        music::leave::leave(),
        music::summon::summon(),
        music::stop::stop(),
        music::skip::skip(),
        music::pause::pause(),
        music::resume::resume(),
        music::shuffle::shuffle(),
        music::seek::seek(),
        music::remove::remove(),
        music::clear::clear(),
        music::repeat::repeat(),
        music::station::station(),
        music::queue::queue(),
        music::now_playing::now_playing(),
        settings::set(),
    ]
}
