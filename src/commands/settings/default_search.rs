use poise::CreateReply;
use serenity::builder::CreateEmbed;

use crate::client::Context;
use crate::database::redis::REDIS;
use crate::handlers::player::Source;
use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;

/// Change the default search source (Default to Spotify)
#[poise::command(
    slash_command,
    prefix_command,
    rename = "default",
    subcommands("_default_search")
)]
pub async fn default_search(_ctx: Context<'_>) -> DotResult<()> {
    Ok(())
}

/// Change the default search source (Default to Spotify)
#[poise::command(slash_command, prefix_command, rename = "search")]
pub async fn _default_search(ctx: Context<'_>, source: Source) -> DotResult<()> {
    let Some(guild_id) = ctx.guild_id() else {
        return Err(DotError::NoServerInformation);
    };

    if matches!(source, Source::Ambiguous) {
        return Err(DotError::Other(
            ":warning: Cannot set default search to **Ambiguous**",
        ));
    }

    REDIS
        .json_set(&guild_id.to_string(), "$.default_search", &source)
        .await
        .unwrap();

    ctx.send(
        CreateReply::new()
            .embed(CreateEmbed::new().description(DotMessages::DefaultSearchChanged(source))),
    )
    .await
    .unwrap();

    Ok(())
}
