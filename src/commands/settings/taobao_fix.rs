use crate::client::Context;
use crate::database::redis::REDIS;
use crate::result::ok::DotMessages;
use crate::result::DotResult;
use enum_display::EnumDisplay;
use poise::{ChoiceParameter, CreateReply};
use serde::{Deserialize, Serialize};

use serenity::all::CreateEmbed;

#[derive(Clone, Copy, Debug, Serialize, Deserialize, ChoiceParameter, EnumDisplay)]
pub enum Toggle {
    On,
    Off,
}
#[poise::command(slash_command, prefix_command)]
pub async fn fix_taobao_url(ctx: Context<'_>, toggle: Toggle) -> DotResult<()> {
    let state = match toggle {
        Toggle::On => true,
        Toggle::Off => false,
    };

    REDIS
        .json_set::<bool>(&ctx.guild_id().unwrap().to_string(), "$.taobao_fix", &state)
        .await
        .unwrap();

    ctx.send(
        CreateReply::new()
            .embed(CreateEmbed::new().description(DotMessages::TaobaoFixToggle(toggle))),
    )
    .await
    .unwrap();

    Ok(())
}
