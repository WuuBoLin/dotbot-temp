pub mod default_search;
pub mod setup_music;
pub mod taobao_fix;
pub mod twitter_fix;

use crate::{client::Context, result::DotResult};

use default_search::default_search;
use setup_music::setup_music;
use taobao_fix::fix_taobao_url;
use twitter_fix::fix_twitter_url;

/// Set a configuration value
#[poise::command(
    slash_command,
    prefix_command,
    subcommands("default_search", "setup_music", "fix_twitter_url", "fix_taobao_url")
)]
pub async fn set(_ctx: Context<'_>) -> DotResult<()> {
    Ok(())
}
