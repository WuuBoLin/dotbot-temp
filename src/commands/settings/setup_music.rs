use crate::client::Context;
use crate::database::redis::REDIS;
use crate::handlers::music_panel::{fresh_panel, MusicPanel};
use poise::CreateReply;

use serenity::builder::{CreateEmbed, CreateMessage};
use serenity::model::channel::Channel;

use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;

/// Setup the Music panel
#[poise::command(
    slash_command,
    prefix_command,
    rename = "music",
    subcommands("_setup_music")
)]
pub async fn setup_music(_ctx: Context<'_>) -> DotResult<()> {
    Ok(())
}

/// Setup the Music panel
#[poise::command(slash_command, prefix_command, rename = "panel")]
pub async fn _setup_music(ctx: Context<'_>, channel: Channel) -> DotResult<()> {
    let message = channel
        .id()
        .send_message(ctx.http(), CreateMessage::new().embeds(fresh_panel()))
        .await
        .map_err(|_| DotError::MusicSetupFailed)?;

    let music_panel = MusicPanel {
        channel_id: channel.id(),
        message: message.id,
    };

    REDIS
        .json_set::<MusicPanel>(
            &ctx.guild_id().unwrap().to_string(),
            "$.music_panel",
            &music_panel,
        )
        .await
        .unwrap();

    ctx.send(
        CreateReply::new().embed(CreateEmbed::new().description(DotMessages::MusicSetupComplete)),
    )
    .await
    .unwrap();

    Ok(())
}
