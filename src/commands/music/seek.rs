use poise::CreateReply;
use serenity::builder::CreateEmbed;

use crate::client::Context;
use crate::connection::check_same_voice_channel;
use crate::handlers::music_panel::refresh_panel;
use crate::handlers::player::Player;
use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;
use crate::time;
use crate::time::duration_ms_to_hms;

/// Seek the current track
#[poise::command(slash_command, prefix_command)]
pub async fn seek(
    ctx: Context<'_>,
    #[description = "Timestamp in the format HH:MM:SS"] timestamp: String,
) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    let position_ms = time::duration_hms_to_ms(timestamp)?;

    let Some(player) = Player::get(ctx.guild_id().unwrap()) else {
        return Err(DotError::NotConnected);
    };

    player.seek(position_ms).await?;

    ctx.send(
        CreateReply::new().embed(CreateEmbed::new().description(DotMessages::Seek {
            timestamp: duration_ms_to_hms(position_ms).unwrap(),
        })),
    )
    .await
    .unwrap();

    refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

    Ok(())
}
