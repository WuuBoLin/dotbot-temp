use poise::CreateReply;
use serenity::builder::CreateEmbed;

use crate::client::Context;
use crate::connection::check_same_voice_channel;
use crate::handlers::music_panel::refresh_panel;
use crate::handlers::player::Player;
use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;

/// Pause the current track
#[poise::command(slash_command, prefix_command)]
pub async fn pause(ctx: Context<'_>) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    let Some(player) = Player::get(ctx.guild_id().unwrap()) else {
        return Err(DotError::NotConnected);
    };

    player.pause().await?;

    ctx.send(CreateReply::new().embed(CreateEmbed::new().description(DotMessages::Pause)))
        .await
        .unwrap();

    refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

    Ok(())
}
