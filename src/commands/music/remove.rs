use crate::client::Context;
use crate::connection::check_same_voice_channel;
use crate::database::redis::REDIS;
use crate::handlers::music_panel::refresh_panel;
use crate::handlers::player::sources::get_track_source_url;
use crate::handlers::player::Track;
use crate::messages::REMOVED_QUEUE;
use crate::result::errors::DotError;
use crate::result::DotResult;
use poise::CreateReply;
use serenity::builder::CreateEmbed;
use std::sync::Arc;

/// Removes a track from the queue
#[poise::command(slash_command, prefix_command)]
pub async fn remove(
    ctx: Context<'_>,
    #[description = "Position of the track in the queue (1 is the next track to be played)"] index: u64,
) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    let guild_id = &ctx.guild_id().unwrap().to_string();
    let mut q = REDIS.queue_ctl().await;
    if index > q.length(guild_id).await {
        return Err(DotError::FailRemoveTrack);
    }
    let track = q.get_track(guild_id, index - 1).await.unwrap();
    if q.remove(guild_id, index - 1).await.is_err() {
        return Err(DotError::FailRemoveTrack);
    }

    ctx.send(CreateReply::new().embed(create_remove_enqueued_embed(track)))
        .await
        .unwrap();

    refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

    Ok(())
}

fn create_remove_enqueued_embed(track: Track) -> CreateEmbed {
    let embed = CreateEmbed::default();

    embed.thumbnail(track.thumbnail_url.clone().unwrap()).field(
        REMOVED_QUEUE,
        format!(
            "[**{}**]({})",
            track.title,
            get_track_source_url(&Arc::new(track.clone()))
        ),
        false,
    )
}
