use poise::CreateReply;
use serenity::builder::CreateEmbed;
use std::sync::atomic::Ordering::Relaxed;

use crate::client::Context;
use crate::connection::check_same_voice_channel;
use crate::handlers::music_panel::refresh_panel;
use crate::handlers::player::Player;
use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;

/// Toggle Station Mode
#[poise::command(slash_command, prefix_command)]
pub async fn station(ctx: Context<'_>) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    let Some(player) = Player::get(ctx.guild_id().unwrap()) else {
        return Err(DotError::NotConnected);
    };

    let station_state = player.station.load(Relaxed);
    player.station.store(!station_state, Relaxed);

    let state = match !station_state {
        true => "ON",
        false => "OFF",
    };

    ctx.send(
        CreateReply::new().embed(CreateEmbed::new().description(DotMessages::StationSet(state))),
    )
    .await
    .unwrap();

    player.get_recommendations()?;
    refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

    Ok(())
}
