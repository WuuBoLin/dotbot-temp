use crate::client::Context;
use crate::connection::get_voice_channel_for_user;
use crate::handlers::player::Player;

use crate::result::ok::DotMessages;
use crate::result::DotResult;

use poise::CreateReply;
use serenity::builder::CreateEmbed;
use serenity::model::mention::Mention;

/// Let me get into a voice channel.
#[poise::command(slash_command, prefix_command)]
pub async fn summon(ctx: Context<'_>) -> DotResult<()> {
    Player::new(ctx).await?.save();

    let guild = ctx.guild().unwrap().clone();
    let author = ctx.author().id;

    ctx.send(
        CreateReply::new().embed(CreateEmbed::new().description(DotMessages::Summon {
            mention: Mention::Channel(get_voice_channel_for_user(&guild, &author).unwrap()),
        })),
    )
    .await
    .unwrap();

    Ok(())
}
