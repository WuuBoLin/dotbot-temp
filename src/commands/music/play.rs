use crate::client::Context;
use crate::connection::{check_voice_connections, Connection};
use crate::handlers::music_panel::refresh_panel;
use crate::handlers::player::extract::extract;
use poise::{AutocompleteChoice, CreateReply};
use serenity::builder::CreateEmbed;
use serenity::prelude::Mentionable;
use std::env;

use crate::database::redis::REDIS;
use rspotify::clients::BaseClient;
use rspotify::model::{SearchResult, SearchType};
use serenity::all::UserId;

use crate::handlers::player::sources::spotify::SpotifyApi;
use crate::handlers::player::sources::youtube::YouTube;
use crate::handlers::player::{Mode, Player, Source};
use crate::messages;
use crate::messages::PLAY_QUEUE;

use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;
use crate::time::duration_ms_to_hms;

/// Play a song or add tracks to the queue
#[poise::command(slash_command, prefix_command)]
pub async fn play(
    ctx: Context<'_>,
    #[description = "Media to play"]
    #[autocomplete = "autocomplete_search"]
    query: String,
    #[description = "Play mode selection"] mode: Option<Mode>,
    #[description = "YouTube or Spotify"] source: Option<Source>,
) -> DotResult<()> {
    let owner_id = env::var("OWNER_ID").expect("Fatality! OWNER_ID not set!");
    if ctx.author().id.to_string() != owner_id {
        match check_voice_connections(ctx) {
            Connection::Bot(channel_id) => {
                return Err(DotError::AuthorDisconnected(channel_id.mention()))
            }
            Connection::Separate(channel_id, _) => {
                return Err(DotError::AlreadyConnected(channel_id.mention()))
            }
            Connection::Neither => return Err(DotError::NotConnected),
            _ => {}
        }
    }

    let guild_id = ctx.guild_id().unwrap();

    let reply_handle = ctx
        .send(CreateReply::new().embed(CreateEmbed::new().description(DotMessages::Search)))
        .await
        .unwrap();

    let tracks = extract(query, (ctx.author().id, guild_id), mode, source).await?;

    let mode = mode.unwrap_or(Mode::End);

    if let Some(player) = Player::get(guild_id) {
        let previous_playing = player.track.load().is_some();
        player.play(tracks, mode).await?;
        if previous_playing {
            if matches!(mode, Mode::Jump) {
                reply_handle
                    .edit(
                        ctx,
                        CreateReply::new().embed(
                            messages::now_playing::now_playing(player, None, false, false).unwrap(),
                        ),
                    )
                    .await
                    .ok();
            } else {
                reply_handle
                    .edit(
                        ctx,
                        CreateReply::new().embed(CreateEmbed::new().description(PLAY_QUEUE)),
                    )
                    .await
                    .ok();
            }
        } else {
            reply_handle
                .edit(
                    ctx,
                    CreateReply::new().embed(
                        messages::now_playing::now_playing(player, None, false, false).unwrap(),
                    ),
                )
                .await
                .ok();
        }

        refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

        return Ok(());
    }

    Player::new(ctx).await?.save();
    let player = Player::get(guild_id).unwrap();
    player.play(tracks, mode).await?;

    reply_handle
        .edit(
            ctx,
            CreateReply::new()
                .embed(messages::now_playing::now_playing(player, None, false, false).unwrap()),
        )
        .await
        .ok();

    refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

    Ok(())
}
async fn autocomplete_search(
    ctx: Context<'_>,
    partial: &str,
) -> impl Iterator<Item = AutocompleteChoice<String>> {
    let guild_id = ctx.guild_id().unwrap();
    let default_source = match REDIS
        .json_get::<Source>(&guild_id.to_string(), "$.default_search")
        .await
    {
        Some(default_search) => default_search,
        None => {
            REDIS
                .json_set(&guild_id.to_string(), "$.default_search", &Source::Spotify)
                .await
                .unwrap();
            Source::Spotify
        }
    };

    match default_source {
        Source::YouTube => complete_youtube(partial).await,
        Source::Spotify => complete_spotify(partial).await,
        Source::Ambiguous => vec![],
    }
    .into_iter()
}

async fn complete_spotify(partial: &str) -> Vec<AutocompleteChoice<String>> {
    let Ok(client) = SpotifyApi::auth().await else {
        return vec![];
    };

    let tracks = client
        .search(partial, SearchType::Track, None, None, Some(6), None)
        .await
        .map(|result| {
            if let SearchResult::Tracks(tracks) = result {
                let tracks = tracks
                    .items
                    .iter()
                    .map(|track| AutocompleteChoice {
                        label: format!(
                            "{} - {} ∙ {}",
                            track.artists.first().unwrap().name,
                            track.name,
                            duration_ms_to_hms(track.duration.num_milliseconds() as u32).unwrap()
                        ),
                        value: track.id.clone().unwrap().to_string(),
                        __non_exhaustive: (),
                    })
                    .collect();
                tracks
            } else {
                vec![]
            }
        })
        .unwrap_or(vec![]);

    let albums = client
        .search(partial, SearchType::Album, None, None, Some(6), None)
        .await
        .map(|result| {
            if let SearchResult::Albums(albums) = result {
                let albums = albums
                    .items
                    .iter()
                    .map(|album| AutocompleteChoice {
                        label: format!("{} - {}", album.artists.first().unwrap().name, album.name),
                        value: album.id.clone().unwrap().to_string(),
                        __non_exhaustive: (),
                    })
                    .collect();
                albums
            } else {
                vec![]
            }
        })
        .unwrap_or(vec![]);

    let playlists = client
        .search(partial, SearchType::Playlist, None, None, Some(6), None)
        .await
        .map(|result| {
            if let SearchResult::Playlists(playlists) = result {
                let playlists = playlists
                    .items
                    .iter()
                    .map(|playlist| {
                        let track_or_tracks = if playlist.tracks.total > 1 {
                            "Tracks"
                        } else {
                            "Track"
                        };
                        AutocompleteChoice {
                            label: format!(
                                "{} - {} ∙ {} {} ",
                                playlist
                                    .owner
                                    .display_name
                                    .clone()
                                    .unwrap_or("Playlist".to_string()),
                                playlist.name,
                                playlist.tracks.total,
                                track_or_tracks
                            ),
                            value: playlist.id.clone().to_string(),
                            __non_exhaustive: (),
                        }
                    })
                    .collect();
                playlists
            } else {
                vec![]
            }
        })
        .unwrap_or(vec![]);

    let mut completion_items = Vec::with_capacity(18);

    let mut iter1 = tracks.into_iter();
    let mut iter2 = albums.into_iter();
    let mut iter3 = playlists.into_iter();

    while iter1.next().is_some() || iter2.next().is_some() || iter3.next().is_some() {
        if let Some(item) = iter1.next() {
            completion_items.push(item);
        }

        if let Some(item) = iter2.next() {
            completion_items.push(item);
        }

        if let Some(item) = iter3.next() {
            completion_items.push(item);
        }
    }

    completion_items
}

async fn complete_youtube(partial: &str) -> Vec<AutocompleteChoice<String>> {
    YouTube::search(partial, 18, UserId::new(1))
        .await
        .map(|tracks| {
            let tracks = tracks
                .iter()
                .map(|track| AutocompleteChoice {
                    label: track.title.clone(),
                    value: track.id.clone(),
                    __non_exhaustive: (),
                })
                .collect();
            tracks
        })
        .unwrap_or(vec![])
}
