use poise::CreateReply;
use serenity::builder::CreateEmbed;

use crate::client::Context;
use crate::connection::check_same_voice_channel;
use crate::database::redis::REDIS;
use crate::handlers::music_panel::refresh_panel;
use crate::result::ok::DotMessages;
use crate::result::DotResult;

/// Shuffle the queue
#[poise::command(slash_command, prefix_command)]
pub async fn shuffle(ctx: Context<'_>) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    REDIS.shuffle(&ctx.guild_id().unwrap().to_string()).await?;

    ctx.send(CreateReply::new().embed(CreateEmbed::new().description(DotMessages::Shuffle)))
        .await
        .unwrap();

    refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

    Ok(())
}
