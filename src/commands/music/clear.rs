use poise::CreateReply;
use serenity::builder::CreateEmbed;

use crate::client::Context;
use crate::connection::check_same_voice_channel;
use crate::database::redis::REDIS;
use crate::handlers::music_panel::refresh_panel;
use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;

/// Clear the queue
#[poise::command(slash_command, prefix_command)]
pub async fn clear(ctx: Context<'_>) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    let mut q = REDIS.queue_ctl().await;
    if (q.clear(&ctx.guild_id().unwrap().to_string()).await).is_err() {
        return Err(DotError::Other("Failed to clear queue"));
    }

    ctx.send(CreateReply::new().embed(CreateEmbed::new().description(DotMessages::Clear)))
        .await
        .unwrap();

    refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

    Ok(())
}
