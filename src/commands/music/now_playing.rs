use poise::CreateReply;

use crate::client::Context;
use crate::connection::check_same_voice_channel;
use crate::handlers::player::Player;
use crate::messages;
use crate::result::errors::DotError;
use crate::result::DotResult;

/// Displays information about the current track
#[poise::command(slash_command, rename = "now", subcommands("_now_playing"))]
pub async fn now_playing(_ctx: Context<'_>) -> DotResult<()> {
    Ok(())
}

/// Displays information about the current track
#[poise::command(slash_command, rename = "playing")]
pub async fn _now_playing(ctx: Context<'_>) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    let Some(player) = Player::get(ctx.guild_id().unwrap()) else {
        return Err(DotError::NotConnected);
    };

    let embed = messages::now_playing::now_playing(player, None, false, false)?;

    ctx.send(CreateReply::new().embed(embed)).await.unwrap();

    Ok(())
}
