use poise::CreateReply;
use serenity::builder::CreateEmbed;

use crate::client::Context;
use crate::connection::check_same_voice_channel;
use crate::handlers::music_panel::refresh_panel;
use crate::handlers::player::{Player, RepeatMode};
use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;

/// Set the looping state of the player
#[poise::command(slash_command, prefix_command)]
pub async fn repeat(
    ctx: Context<'_>,
    #[description = "Repeat mode selection"] mode: Option<RepeatMode>,
) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    let Some(player) = Player::get(ctx.guild_id().unwrap()) else {
        return Err(DotError::NotConnected);
    };

    let repeat_mode = player.repeat(mode);

    if matches!(repeat_mode, RepeatMode::None) {
        ctx.send(
            CreateReply::new().embed(CreateEmbed::new().description(DotMessages::LoopDisable)),
        )
        .await
        .unwrap();
        return Ok(());
    }

    ctx.send(
        CreateReply::new()
            .embed(CreateEmbed::new().description(DotMessages::LoopSet { mode: repeat_mode })),
    )
    .await
    .unwrap();

    refresh_panel(ctx.serenity_context(), ctx.guild_id()).await;

    Ok(())
}
