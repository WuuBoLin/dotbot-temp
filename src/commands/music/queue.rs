use crate::client::Context;
use crate::connection::check_same_voice_channel;

use crate::messages::queue::build_queue_page;
use crate::result::errors::DotError;
use crate::result::DotResult;
use ::serenity::{
    builder::{
        CreateActionRow, CreateButton, CreateInteractionResponse, CreateInteractionResponseMessage,
    },
    collector::ComponentInteractionCollector,
};
use poise::CreateReply;

/// Displays information about the current track
#[poise::command(slash_command, prefix_command)]
pub async fn queue(ctx: Context<'_>, page: Option<u64>) -> DotResult<()> {
    check_same_voice_channel(ctx)?;

    let Some(guild_id) = ctx.guild_id() else {
        return Err(DotError::NoServerInformation);
    };

    let mut queue_list = build_queue_page(guild_id, page.map(|p| p - 1).unwrap_or(0), true).await?;

    let ctx_id = ctx.id();
    let home_button_id = format!("{}home", ctx.id());
    let prev_button_id = format!("{}prev", ctx.id());
    let next_button_id = format!("{}next", ctx.id());
    let end_button_id = format!("{}end", ctx.id());

    ctx.send(
        CreateReply::new()
            .embed(queue_list.embed.unwrap())
            .components(vec![CreateActionRow::Buttons(vec![
                CreateButton::new(home_button_id.clone())
                    .label("<<")
                    .disabled(queue_list.current_page == 1),
                CreateButton::new(prev_button_id.clone())
                    .label("<")
                    .disabled(queue_list.current_page == 1),
                CreateButton::new(next_button_id.clone())
                    .label(">")
                    .disabled(queue_list.current_page == queue_list.total_pages),
                CreateButton::new(end_button_id.clone())
                    .label(">>")
                    .disabled(queue_list.current_page == queue_list.total_pages),
            ])]),
    )
    .await
    .ok();

    // Loop through incoming interactions with the navigation buttons
    while let Some(press) = ComponentInteractionCollector::new(&ctx.serenity_context().shard)
        .filter(move |press| press.data.custom_id.starts_with(&ctx_id.to_string()))
        .timeout(std::time::Duration::from_secs(3600 * 24))
        .next()
        .await
    {
        // Depending on which button was pressed, go to next or previous page
        if press.data.custom_id == next_button_id.clone() {
            queue_list.current_page += 1;
        } else if press.data.custom_id == prev_button_id.clone() {
            queue_list.current_page = if queue_list.current_page == 1 {
                queue_list.total_pages
            } else {
                queue_list.current_page - 1
            };
        } else if press.data.custom_id == home_button_id {
            queue_list.current_page = 1;
        } else if press.data.custom_id == end_button_id.clone() {
            queue_list.current_page = queue_list.total_pages;
        } else {
            // This is an unrelated button interaction
            continue;
        }

        let queue_list = build_queue_page(guild_id, queue_list.current_page - 1, true).await?;

        // Update the message with the new page contents
        press
            .create_response(
                ctx.http(),
                CreateInteractionResponse::UpdateMessage(
                    CreateInteractionResponseMessage::new()
                        .embed(queue_list.embed.unwrap())
                        .components(vec![CreateActionRow::Buttons(vec![
                            CreateButton::new(home_button_id.clone())
                                .label("<<")
                                .disabled(queue_list.current_page == 1),
                            CreateButton::new(prev_button_id.clone())
                                .label("<")
                                .disabled(queue_list.current_page == 1),
                            CreateButton::new(next_button_id.clone())
                                .label(">")
                                .disabled(queue_list.current_page == queue_list.total_pages),
                            CreateButton::new(end_button_id.clone())
                                .label(">>")
                                .disabled(queue_list.current_page == queue_list.total_pages),
                        ])]),
                ),
            )
            .await
            .ok();

        tokio::time::sleep(std::time::Duration::from_millis(100)).await
    }

    Ok(())
}
