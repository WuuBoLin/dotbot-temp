use crate::client::Context;
use crate::result::errors::DotError;
use crate::result::DotResult;
use serenity::model::{
    guild::Guild,
    id::{ChannelId, UserId},
};
use serenity::prelude::Mentionable;
use std::env;

#[derive(Debug)]
pub enum Connection {
    User(ChannelId),
    Bot(ChannelId),
    Mutual(ChannelId, ChannelId),
    Separate(ChannelId, ChannelId),
    Neither,
}

pub fn check_voice_connections(ctx: Context) -> Connection {
    let Some(guild) = &ctx.guild() else {
        return Connection::Neither;
    };
    let user_id = &ctx.author().id;
    let bot_id = &ctx.framework().bot_id;

    let user_channel = get_voice_channel_for_user(guild, user_id);
    let bot_channel = get_voice_channel_for_user(guild, bot_id);

    if let (Some(bot_id), Some(user_id)) = (bot_channel, user_channel) {
        if bot_id.0 == user_id.0 {
            Connection::Mutual(bot_id, user_id)
        } else {
            Connection::Separate(bot_id, user_id)
        }
    } else if let (Some(bot_id), None) = (bot_channel, user_channel) {
        Connection::Bot(bot_id)
    } else if let (None, Some(user_id)) = (bot_channel, user_channel) {
        Connection::User(user_id)
    } else {
        Connection::Neither
    }
}

pub fn get_voice_channel_for_user(guild: &Guild, user_id: &UserId) -> Option<ChannelId> {
    guild
        .voice_states
        .get(user_id)
        .and_then(|voice_state| voice_state.channel_id)
}

pub fn check_same_voice_channel(ctx: Context) -> DotResult<()> {
    let owner_id = env::var("OWNER_ID").expect("Fatality! OWNER_ID not set!");
    if ctx.author().id.to_string() == owner_id {
        return Ok(());
    }

    match check_voice_connections(ctx) {
        Connection::User(_) => Err(DotError::NotConnected),
        Connection::Mutual(_, _) => Ok(()),
        Connection::Bot(channel_id) => Err(DotError::AuthorDisconnected(channel_id.mention())),
        Connection::Separate(channel_id, _) => {
            Err(DotError::AlreadyConnected(channel_id.mention()))
        }
        Connection::Neither => Err(DotError::NotConnected),
    }
}
