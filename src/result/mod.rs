pub mod errors;
pub mod ok;

use crate::result::errors::DotError;

pub type DotResult<T> = Result<T, DotError>;
