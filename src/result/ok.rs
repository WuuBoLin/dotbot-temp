use std::fmt::Display;

use crate::handlers::player::{RepeatMode, Source};
use serenity::model::mention::Mention;

pub use crate::messages::*;

#[derive(Debug)]
pub enum DotMessages {
    AutopauseOff,
    AutopauseOn,
    Clear,
    DefaultSearchChanged(Source),
    Error,
    Leaving,
    LoopDisable,
    LoopSet { mode: RepeatMode },
    StationSet(&'static str),
    NowPlaying,
    Pause,
    PlayAllFailed,
    PlayDomainBanned { domain: String },
    PlaylistQueued,
    RemoveMultiple,
    Resume,
    Search,
    Seek { timestamp: String },
    Shuffle,
    Skip,
    SkipAll,
    SkipTo { title: String, url: String },
    Stop,
    Summon { mention: Mention },
    Success,
    VoteSkip { mention: Mention, missing: usize },
    MusicSetupComplete,
    MusicPlayer,
    NextYearCountdownInSec { year: i32, seconds: i64 },
    TwitterFixToggle(crate::commands::settings::twitter_fix::Toggle),
    TaobaoFixToggle(crate::commands::settings::taobao_fix::Toggle),
}

impl Display for DotMessages {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::AutopauseOff => f.write_str(AUTOPAUSE_OFF),
            Self::AutopauseOn => f.write_str(AUTOPAUSE_ON),
            Self::Clear => f.write_str(CLEARED),
            Self::DefaultSearchChanged(source) => {
                f.write_str(&format!("{} **{}**!", DEFAULT_SEARCH_CHANGED, source))
            }
            Self::Error => f.write_str(ERROR),
            Self::Leaving => f.write_str(LEAVING),
            Self::LoopDisable => f.write_str(LOOP_DISABLED),
            Self::LoopSet { mode } => f.write_str(&format!("{} **{}**", LOOP_SET, mode)),
            Self::StationSet(state) => f.write_str(&format!("{} **{}**", STATION_MODE, state)),
            Self::NowPlaying => f.write_str(QUEUE_NOW_PLAYING),
            Self::Pause => f.write_str(PAUSED),
            Self::PlaylistQueued => f.write_str(PLAY_PLAYLIST),
            Self::PlayAllFailed => f.write_str(PLAY_ALL_FAILED),
            Self::PlayDomainBanned { domain } => {
                f.write_str(&format!("⚠️ **{}** {}", domain, PLAY_FAILED_BLOCKED_DOMAIN))
            }
            Self::Search => f.write_str(SEARCHING),
            Self::RemoveMultiple => f.write_str(REMOVED_QUEUE_MULTIPLE),
            Self::Resume => f.write_str(RESUMED),
            Self::Shuffle => f.write_str(SHUFFLED_SUCCESS),
            Self::Stop => f.write_str(STOPPED),
            Self::Success => f.write_str(SUCCESS),
            Self::VoteSkip { mention, missing } => f.write_str(&format!(
                "{}{} {} {} {}",
                SKIP_VOTE_EMOJI, mention, SKIP_VOTE_USER, missing, SKIP_VOTE_MISSING
            )),
            Self::Seek { timestamp } => f.write_str(&format!("{} **{}**!", SEEKED, timestamp)),
            Self::Skip => f.write_str(SKIPPED),
            Self::SkipAll => f.write_str(SKIPPED_ALL),
            Self::SkipTo { title, url } => {
                f.write_str(&format!("{} [**{}**]({})!", SKIPPED_TO, title, url))
            }
            Self::Summon { mention } => f.write_str(&format!("{} **{}**!", JOINING, mention)),
            Self::MusicSetupComplete => f.write_str(MUSIC_SETUP_COMPLETE),
            Self::MusicPlayer => f.write_str(MUSIC_PLAYER),
            Self::NextYearCountdownInSec { year, seconds } => f.write_str(&format!(
                "⏰ `{}` {} `{}`",
                seconds, COUNT_DOWN_TO_NEXT_YEAR, year
            )),
            Self::TwitterFixToggle(state) => {
                f.write_str(&format!("{} **{}**!", TWITTER_FIX_TOGGLE, state))
            }
            Self::TaobaoFixToggle(state) => {
                f.write_str(&format!("{} **{}**!", TAOBAO_FIX_TOGGLE, state))
            }
        }
    }
}

impl From<DotMessages> for String {
    fn from(value: DotMessages) -> Self {
        value.to_string()
    }
}
