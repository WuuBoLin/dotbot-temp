pub mod tracing_rpc {
    tonic::include_proto!("tracing");
}

use tonic::{Code, Request, Response, Status};
use tracing::level_filters::LevelFilter;
use tracing::{debug, info};
use tracing_subscriber::{reload, Registry};

use crate::rpc::tracing::tracing_rpc::LevelReply;
use tracing_rpc::tracing_server::Tracing;
use tracing_rpc::LevelRequest;

pub static TRACING_HANDLE: std::sync::OnceLock<reload::Handle<LevelFilter, Registry>> =
    std::sync::OnceLock::new();

#[derive(Default)]
pub struct TracingRpc {}

#[tonic::async_trait]
impl Tracing for TracingRpc {
    async fn set_level(
        &self,
        request: Request<LevelRequest>,
    ) -> Result<Response<LevelReply>, Status> {
        let remote_addr = request
            .remote_addr()
            .map(|addr| addr.to_string())
            .unwrap_or_default();

        info!(
            "Got a tracing level change request from {remote_addr} with argument {:?}",
            request.get_ref()
        );

        let level = match request.get_ref().level.to_uppercase().as_str() {
            "" => LevelFilter::INFO,
            "TRACE" => LevelFilter::TRACE,
            "DEBUG" => LevelFilter::DEBUG,
            "INFO" => LevelFilter::INFO,
            "WARN" => LevelFilter::WARN,
            "ERROR" => LevelFilter::ERROR,
            "OFF" => LevelFilter::OFF,
            _ => {
                debug!(
                    "Invalid level request from {remote_addr}, {:?}",
                    request.get_ref()
                );
                return Err(Status::new(
                    Code::InvalidArgument,
                    "Level should be one of TRACE, DEBUG, INFO, WARN, ERROR, OFF.",
                ));
            }
        };

        let old = TRACING_HANDLE.get().unwrap().clone_current().unwrap();
        if TRACING_HANDLE
            .get()
            .unwrap()
            .modify(|filter| *filter = level)
            .is_err()
        {
            return Err(Status::new(
                Code::Internal,
                "Error while changing trace level.",
            ));
        };

        let new_level = level.to_string().to_uppercase();
        info!("Tracing level sets to {}", new_level);

        let reply = LevelReply {
            old: old.to_string().to_uppercase(),
            new: new_level,
        };

        Ok(Response::new(reply))
    }
}
