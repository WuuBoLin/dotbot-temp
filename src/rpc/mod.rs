pub mod tracing;

use crate::rpc::tracing::tracing_rpc::tracing_server::TracingServer;
use crate::rpc::tracing::TracingRpc;
use ::tracing::error;

pub async fn activate() {
    tokio::task::spawn(async {
        let addr = "[::]:121".parse().unwrap();
        let btc_service = TracingRpc::default();

        if let Err(reason) = tonic::transport::Server::builder()
            .add_service(TracingServer::new(btc_service))
            .serve(addr)
            .await
        {
            error!("Failed to activate RPC server!!\nReason: {:#?}", reason)
        }
    });
}
