use crate::client::BOT_AVATAR;
use crate::handlers::player::sources::get_track_source_url;
use crate::handlers::player::{Player, Source};
use crate::result::errors::DotError;
use crate::result::ok::DotMessages;
use crate::result::DotResult;
use crate::time::duration_ms_to_hms;
use ::serenity::builder::{CreateEmbedAuthor, CreateEmbedFooter};
use poise::serenity_prelude as serenity;
use serenity::builder::CreateEmbed;
use serenity::Colour;
use std::sync::atomic::Ordering::Relaxed;
use std::sync::Arc;

pub fn now_playing(
    player: Arc<Player>,
    author_name: Option<DotMessages>,
    inline_requestedby: bool,
    no_progress: bool,
) -> DotResult<CreateEmbed> {
    let Some(track) = player.track.load_full() else {
        return Err(DotError::NothingPlaying);
    };

    let track_duration = if track.is_live {
        "∞".to_string()
    } else {
        duration_ms_to_hms(track.duration_ms)?
    };
    let track_progress = if player.playing.load(Relaxed) {
        player.progress()?
    } else {
        duration_ms_to_hms(track.start_from)?
    };
    let progress = format!("{} / {}", track_progress, track_duration);
    let artists = track.artists.to_vec().join(", ");
    let thumbnail_url = track
        .thumbnail_url
        .clone()
        .unwrap_or(BOT_AVATAR.to_string());
    let (footer_text, footer_icon_url) = match track.source {
        Source::YouTube => (
            "Streaming via YouTube",
            "https://www.google.com/s2/favicons?domain=youtube.com",
        ),
        Source::Spotify => (
            "Streaming via Spotify",
            "https://www.google.com/s2/favicons?domain=spotify.com",
        ),
        Source::Ambiguous => ("Streaming via an Ambiguous Source", BOT_AVATAR),
    };

    let embed = CreateEmbed::default();

    Ok(embed
        .author(CreateEmbedAuthor::new(
            author_name.unwrap_or(DotMessages::NowPlaying),
        ))
        .title(track.title.clone())
        .url(get_track_source_url(&track))
        .thumbnail(thumbnail_url)
        .field(
            if no_progress { "Duration" } else { "Progress" },
            format!(
                ">>> {}",
                if no_progress {
                    duration_ms_to_hms(track.duration_ms)?
                } else {
                    progress
                }
            ),
            true,
        )
        .field("Artists", format!(">>> {}", artists), true)
        .field(
            "Requested by",
            format!("<@{}>", track.requested_by),
            inline_requestedby,
        )
        .color(Colour::from_rgb(88, 101, 242))
        .footer(CreateEmbedFooter::new(footer_text).icon_url(footer_icon_url)))
}
