use crate::client::BOT_AVATAR;
use crate::database::redis::REDIS;
use crate::handlers::player::sources::get_track_source_url;
use crate::handlers::player::{Player, Track};
use crate::messages::{QUEUE_NOW_PLAYING, QUEUE_UP_NEXT};
use crate::result::errors::DotError;
use crate::result::DotResult;
use crate::time::duration_ms_to_hms;
use ::serenity::builder::CreateEmbedFooter;
use poise::serenity_prelude as serenity;
use serenity::builder::CreateEmbed;
use serenity::model::id::GuildId;
use serenity::Colour;
use std::cmp::max;
use std::fmt::Write;
use std::sync::Arc;

pub const EMBED_PAGE_SIZE: u64 = 5;

pub struct QueueList {
    pub content: String,
    pub length: u64,
    pub current_page: u64,
    pub total_pages: u64,
    pub embed: Option<CreateEmbed>,
}

pub async fn build_queue_page(
    guild_id: GuildId,
    page: u64,
    now_playing: bool,
) -> DotResult<QueueList> {
    let track = if let Some(player) = Player::get(guild_id) {
        if player.track.load_full().is_none() {
            return Err(DotError::NothingPlaying);
        };
        player.track.load_full().unwrap()
    } else {
        return Err(DotError::NotConnected);
    };
    let Some(queue_list) = build_queue_list(guild_id, page).await else {
        return Err(DotError::QueueEmpty);
    };
    let embed = CreateEmbed::default();
    let embed = if now_playing {
        let now_playing = format!(
            "[{}]({}) • `{}`",
            track.title,
            get_track_source_url(&track),
            duration_ms_to_hms(track.duration_ms).unwrap()
        );
        embed.field(QUEUE_NOW_PLAYING, now_playing, false).field(
            QUEUE_UP_NEXT,
            &queue_list.content,
            false,
        )
    } else {
        embed.field(QUEUE_UP_NEXT, &queue_list.content, false)
    };
    let embed = if queue_list.total_pages <= 1 && !now_playing {
        embed
    } else {
        let text = if queue_list.total_pages > 1 && now_playing {
            format!(
                "Page {}/{} • {} left...",
                queue_list.current_page,
                queue_list.total_pages,
                queue_list
                    .length
                    .saturating_sub(queue_list.current_page * EMBED_PAGE_SIZE)
            )
        } else if queue_list.total_pages > 1 && !now_playing {
            format!(
                "{} left...",
                queue_list
                    .length
                    .saturating_sub(queue_list.current_page * EMBED_PAGE_SIZE)
            )
        } else {
            String::from("Dot")
        };

        if queue_list.total_pages > 1 || now_playing {
            let footer = CreateEmbedFooter::new(text).icon_url(BOT_AVATAR);
            embed
                .clone()
                .footer(footer)
                .color(Colour::from_rgb(88, 101, 242))
        } else {
            let footer = CreateEmbedFooter::new(text);
            embed
                .clone()
                .footer(footer)
                .color(Colour::from_rgb(88, 101, 242))
        }
    };
    Ok(QueueList {
        content: queue_list.content,
        length: queue_list.length,
        current_page: queue_list.current_page,
        total_pages: queue_list.total_pages,
        embed: Some(embed),
    })
}

async fn build_queue_list(guild_id: GuildId, page: u64) -> Option<QueueList> {
    let Some(queue) = get_queue_in_page(guild_id, page).await else {
        return None;
    };
    let mut q = REDIS.queue_ctl().await;
    let length = q.length(&guild_id.to_string()).await;
    let (start_index, _) = calc_page_range(length, page).unwrap();

    let mut description = String::new();

    for (i, t) in queue.iter().enumerate() {
        let title = t.title.clone();
        let url = get_track_source_url(&Arc::new(t.clone()));
        let duration = duration_ms_to_hms(t.duration_ms).unwrap();

        let _ = writeln!(
            description,
            "`{}.` [{}]({}) • `{}`",
            i as u64 + start_index + 1,
            title,
            url,
            duration
        );
    }

    Some(QueueList {
        content: description,
        length,
        current_page: page + 1,
        total_pages: calc_num_pages(length),
        embed: None,
    })
}

pub async fn get_queue_in_page(guild_id: GuildId, page: u64) -> Option<Vec<Track>> {
    let mut q = REDIS.queue_ctl().await;
    let length = q.length(&guild_id.to_string()).await;
    let num_pages = calc_num_pages(length);
    if page > num_pages {
        return None;
    }
    let Some((start_index, end_index)) = calc_page_range(length, page) else {
        return None;
    };
    q.get_tracks(&guild_id.to_string(), start_index, end_index)
        .await
}

fn calc_num_pages(length: u64) -> u64 {
    let num_pages = ((length as f64 - 1.0) / EMBED_PAGE_SIZE as f64).ceil() as u64;
    max(1, num_pages)
}

fn calc_page_range(length: u64, page_num: u64) -> Option<(u64, u64)> {
    let page_size = 5;
    let start_index = page_num * page_size;
    let end_index = start_index + page_size;
    if start_index < length {
        Some((start_index, end_index.min(length)))
    } else {
        None
    }
}
