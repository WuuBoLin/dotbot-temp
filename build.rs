fn main() -> Result<(), Box<dyn std::error::Error>> {
    let proto_file = "./src/rpc/protos/tracing.proto";

    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional") // for older systems
        .build_client(true)
        .build_server(true)
        .compile(&[proto_file], &["proto"])?;

    Ok(())
}
